//
//  CartViewController.m
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "CartViewController.h"

@implementation CartViewController
-(id)init{
    self = [super init];
    if(self) {
        
        [self setTitle:NSLocalizedString(@"CHECKOUT", nil)];
        
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
     /* END View Controller Styling */
    
    /* Code START Here */
    
    scroller = [MGScrollView scrollerWithSize:self.view.size];
     scroller.alwaysBounceVertical = YES;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame), 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    [self.view addSubview:scroller];
    
    //Register to global
    [[ViewControllerHelper instance] set_cart_controller:self];
    
    /* ADD LABEL */
    label_instruction = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, self.view.size.height)];
    label_instruction.text = NSLocalizedString(@"CART IS EMPTY", nil);
    label_instruction.textAlignment = NSTextAlignmentCenter;
    label_instruction.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:16];
    label_instruction.textColor = [UIColor lightGrayColor];
    
    [self.view addSubview:label_instruction];
    
    /* Code END */
}

- (void)viewDidAppear:(BOOL)animated{
    [[ViewControllerHelper instance] get_tabbar_controller].navigationItem.title=NSLocalizedString(@"CHECKOUT", nil);
    [self add_content];
}

-(void)refresh_content{
    [scroller.boxes removeAllObjects];
    [scroller layoutWithDuration:0.3 completion:nil];
}

-(void)add_content{
    
    NSMutableArray *array = [[CartHelper instance] get_cart];
    
     [scroller.boxes removeAllObjects];
    
    if(array.count > 0)
    {
       
    for(int i=0;i<array.count;i++)
    {
        NSDictionary *data = [array objectAtIndex:i];
        
        NSDictionary *raw_data = [data objectForKey:@"data"];
         NSString *name = [raw_data objectForKey:@"name"];
        NSString *value = [data objectForKey:@"value"];
          NSString *frequency = [data objectForKey:@"frequency"];
        [self single_box:name value:value frequency:frequency object_id:i];
    }
    [self total_box:[NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]]];
        
    [self next_btn];
    [scroller layoutWithDuration:0.3 completion:nil];
        label_instruction.hidden = YES;
    }
    else
    {
        
        label_instruction.hidden = NO;
    }
}



-(void)next_btn{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 70)];
    
    HyLoglnButton *next_btn = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 30, box.width-20, 40)];
    [next_btn setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:next_btn];
    [next_btn setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateNormal];
    [next_btn addTarget:self action:@selector(next_btn_action:) forControlEvents:UIControlEventTouchUpInside];
    
    [scroller.boxes addObject:box];
    
}


-(void)next_btn_action:(HyLoglnButton *)button{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //Check user if already login
        if([[UserDataService instance] check_user_if_already_logged] == false)
        {
            alertLogin = [[SCLAlertView alloc] initWithNewWindow];
            SCLAlertView *alertLogin2 = alertLogin;
            alertLogin.backgroundType = Blur;
            
           // alert.customViewColor = [[Appearances instance] nav_bar_color];
            email = [alertLogin addTextField:@"Type your email"];
           [email setKeyboardType:UIKeyboardTypeEmailAddress];
            
            password = [alertLogin addTextField:@"Type your password"];
             password.secureTextEntry = YES;
            SCLButton *login_btn = [[SCLButton alloc] init];
            
            login_btn = [alertLogin addButton:@"LOGIN" validationBlock:0  actionBlock:^(void) {
              
                [self login:button];
            }];
            
            SCLButton *facebook_btn = [[SCLButton alloc] init];
            
            facebook_btn= [alertLogin addButton:@"LOGIN WITH FACEBOOK" actionBlock:^(void) {
                
                [self login_with_facebook:button];
            }];
     
            
            [alertLogin addButton:@"CANCEL" actionBlock:^(void) {
                   [alertLogin2 hideView];
                [button RevertAnimationCompletion:^{
                    
                    
                    
                }];
            }];
            
            
            FAKFontAwesome *logged_Icon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:18 error:nil];
            [logged_Icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
            UIImage *logged_Icon_image = [logged_Icon imageWithSize:CGSizeMake(18, 18)];
            
            [alertLogin showCustom:logged_Icon_image color:[[Appearances instance] nav_bar_color] title:@"LOGIN" subTitle:@"Please login before make any payment" closeButtonTitle:nil duration:0.0f];
            facebook_btn.defaultBackgroundColor = [UIColor colorWithRed:0.23 green:0.35 blue:0.6 alpha:1.0];
            
            
                
            
            
        }
        else
        {
            PaymentMethodViewController *choose_payment = [[PaymentMethodViewController alloc] init];
            [[[ViewControllerHelper instance] get_tabbar_controller].navigationController pushViewController:choose_payment animated:YES];
            [button RevertAnimationCompletion:^{
                
            }];
            
            
            
            
        }
        
        
    });
    
}


-(void)login:(HyLoglnButton *)button{
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    if([email.text isEqualToString:@""])
    {
        
     
    }
    else if([password.text isEqualToString:@""])
    {
       
    }
    else
    {
        
        
        [alertLogin hideView];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSDictionary *user_data = [[UserDataService instance] login:email.text password:password.text];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if([[user_data objectForKey:@"status_code"] intValue] == 0)
                {
                    
                    
                    //Register userdata in global
                    [[UserDataService instance] set_user_data:[user_data objectForKey:@"response"]];
                    
                    LoggedViewController *log = [[LoggedViewController alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
                    
                    FAKFontAwesome *myProfileIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
                    UIImage *myProfileIcon_image = [myProfileIcon imageWithSize:CGSizeMake(24, 24)];
                    nav.tabBarItem.image = myProfileIcon_image;
                    
                    UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
                    NSMutableArray *as = [tab.viewControllers mutableCopy];
                    [as replaceObjectAtIndex:1 withObject:nav];
                    
                    [[ViewControllerHelper instance] get_tabbar_controller].viewControllers = as;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [button RevertAnimationCompletion:^{
                            
                        }];
                        
                        PaymentMethodViewController *choose_payment = [[PaymentMethodViewController alloc] init];
                        [[[ViewControllerHelper instance] get_tabbar_controller].navigationController pushViewController:choose_payment animated:YES];
                        
                        
                        
                    });
                    
                }
                else
                {
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [button ErrorRevertAnimationCompletion:^{
                            
                            
                            
                        }];
                        
                        [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Your email/password is wrong", nil)closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
                    });
                    
                    
                    
                }
                
                
            });
            
        });
        
        
    }
}


-(void)login_with_facebook:(HyLoglnButton *)button{
    
    
    [alertLogin hideView];
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:[[ViewControllerHelper instance] get_my_profile_controller_controller]
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             
             [button RevertAnimationCompletion:^{
                 
                 
                 
             }];
             
             [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Sorry error was occured while connecting with facebook. Please try again.", nil)closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
             
             
         } else if (result.isCancelled) {
             
             [button RevertAnimationCompletion:^{
                 
                 
                 
             }];
             
             
         } else {
             [self fetchUserInfo:button];
         }
     }];
    
    
    
    
}

-(void)fetchUserInfo:(HyLoglnButton *)button
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             
             if (!error)
             {
                 NSString *fb_name = [result objectForKey:@"name"];
                 NSString *fb_email = [result objectForKey:@"email"];
                 NSString *fb_userId = [result objectForKey:@"id"];
                 
                 
                 [self fb_server_login:button fb_id:fb_userId fb_email:fb_email fb_name:fb_name];
             }
             
         }];
    }
}

-(void)fb_server_login:(HyLoglnButton *)button fb_id:(NSString*)fb_id fb_email:(NSString*)fb_email fb_name:(NSString*)fb_name {
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSDictionary *user_data = [[UserDataService instance] login_fb:fb_id fb_email:fb_email fb_name:fb_name];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if([[user_data objectForKey:@"status_code"] intValue] == 0)
            {
                PaymentMethodViewController *choose_payment = [[PaymentMethodViewController alloc] init];
                [[[ViewControllerHelper instance] get_tabbar_controller].navigationController pushViewController:choose_payment animated:YES];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                   
                    
                    
                    [button ExitAnimationCompletion:^{
                        //Register userdata in global
                        [[UserDataService instance] set_user_data:[user_data objectForKey:@"response"]];
                        
                       
                        
                        
                        LoggedViewController *log = [[LoggedViewController alloc] init];
                        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
                        
                        FAKFontAwesome *myProfileIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
                        UIImage *myProfileIcon_image = [myProfileIcon imageWithSize:CGSizeMake(24, 24)];
                        nav.tabBarItem.image = myProfileIcon_image;
                        
                        UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
                        NSMutableArray *as = [tab.viewControllers mutableCopy];
                        [as replaceObjectAtIndex:1 withObject:nav];
                        
                        [[ViewControllerHelper instance] get_tabbar_controller].viewControllers = as;
                        
                        
                        
                        
                        
                        
                    }];
                });
                
            }
            
            
        });
        
    });
    
    
}



-(void)total_box:(NSString*)value{
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_sum_layout:self.view.size value:value];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    
}


-(void)single_box:(NSString*)name value:(NSString*)value frequency:(NSString*)frequency object_id:(int)object_id{
    
    
    int freq_int = [frequency intValue];
    NSString *freq_word = [[[Appearances instance] frequecy_variable] objectAtIndex:freq_int];
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_layout:self.view.size charity_name:name value:value frequency:freq_word];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    /* START cart delete btn */
    FAKFontAwesome *trash_icon = [FAKFontAwesome  iconWithIdentifier:@"fa-trash" size:24 error:nil];
    trash_icon.drawingBackgroundColor = [UIColor redColor];
    [trash_icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *trash_icon_image = [trash_icon imageWithSize:CGSizeMake(53, 53)];
    
    UIImageView *deleteIcon = [[UIImageView alloc] initWithImage:trash_icon_image];
    deleteIcon.frame = CGRectMake(self.view.width, 0, 53, 53);
    deleteIcon.hidden = NO;
    deleteIcon.userInteractionEnabled = YES;
    [box addSubview:deleteIcon];
     box.swiper.direction = UISwipeGestureRecognizerDirectionLeft;
       LayoutBoxHelper *box2 = box;
    
    //Delete Cart Action Btn
    UserDataTapGestureRecognizer *singleTap = [[UserDataTapGestureRecognizer alloc] initWithTarget:self action:@selector(delete_cart_item:)];
    singleTap.userData = [NSString stringWithFormat:@"%d",object_id];
    [deleteIcon addGestureRecognizer:singleTap];
    
    box.onSwipe = ^{
        if(box2.swiper.direction == UISwipeGestureRecognizerDirectionLeft)
        {
            
            box2.swiper.direction = UISwipeGestureRecognizerDirectionRight;
            
            
            [UIView animateWithDuration:0.2 animations:^{
               deleteIcon.x = self.view.width-53;
              
            }];
        }
        else
        {
            
            
            box2.swiper.direction = UISwipeGestureRecognizerDirectionLeft;
            [UIView animateWithDuration:0.2 animations:^{
                deleteIcon.x = self.view.width+53;
            } completion:^ (BOOL finished)
             {
                
                 
             }];
        }
        
        
        
    };
    
    
    
    
    /* END cart delete btn */
}



-(void)delete_cart_item:(UserDataTapGestureRecognizer*)tap{
    
    NSLog(@"Delete Cart");
    NSString *object_id = [NSString stringWithFormat:@"%@",tap.userData];
    
    [[CartHelper instance] delete_item:[object_id intValue]];
    
    //Refresh Cart
    [self add_content];
    [scroller layoutWithDuration:0.3 completion:nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
