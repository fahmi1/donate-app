//
//  ToolHelper.h
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToolHelper : NSObject
+ (ToolHelper *) instance;
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize source:(UIImage*)sourceImages;
- (BOOL) validateEmail: (NSString *) email;
@end
