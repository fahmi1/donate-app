//
//  WebViewController.m
//  donateapp
//
//  Created by Dead Mac on 04/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController
@synthesize webview;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, self.view.size.height)];
    
    NSURL *nsurl=[NSURL URLWithString:url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webview loadRequest:nsrequest];
    
    for(UIView *wview in [[[webview subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]]) { wview.hidden = YES; }
    }
    webview.backgroundColor = [UIColor clearColor];
    [webview setOpaque:NO];
    
    
    [self.view addSubview:webview];
    
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:@"CLOSE" style:UIBarButtonItemStylePlain target:self action:@selector(closeAction)];
    self.navigationItem.rightBarButtonItem = close;
    
}
-(void)closeAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)set_url:(NSString*)urlstring{
    url = urlstring;
}

@end
