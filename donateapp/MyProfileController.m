//
//  MyProfileController.m
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "MyProfileController.h"

@interface MyProfileController ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation MyProfileController

-(id)init{
    self = [super init];
    if(self) {
        
        [[ViewControllerHelper instance] get_main_navigation].title = NSLocalizedString(@"LOGIN", nil);
        [self setTitle:NSLocalizedString(@"LOGIN", nil)];
        
        
    }
    
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [[ViewControllerHelper instance] get_tabbar_controller].navigationItem.title=NSLocalizedString(@"LOGIN", nil);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    
    
    /* START CODE */
    
    //Register into Helper
    [[ViewControllerHelper instance] set_my_profile_controller:self];
   
    NSMutableArray *controllerArray = [NSMutableArray array];
    
    LoginViewController *login_controller = [[LoginViewController alloc] init];
    login_controller.title = NSLocalizedString(@"LOGIN", nil);
    [controllerArray addObject:login_controller];
    
    RegisterViewController *register_conroller = [[RegisterViewController alloc] init];
    register_conroller.title = NSLocalizedString(@"REGISTER", nil);
    [controllerArray addObject:register_conroller];
    
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor redColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:[[Appearances instance] font_family_regular] size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(90.0),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    // Initialize page menu with controller array, frame, and optional parameters
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 63, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    
     /* END CODE */
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
