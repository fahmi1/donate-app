//
//  ListItemController.m
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "ListItemController.h"

@interface ListItemController ()

@end

@implementation ListItemController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* Code START Here */
    
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(-2, -1, self.view.width+4, self.view.height);
    scroller.alwaysBounceVertical = YES;
    scroller.contentLayoutMode = MGLayoutGridStyle;
    fixedHeight = CGRectGetHeight(scroller.frame);
    currentPage = 1;
    processing = NO;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame)+100, 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    
    [self.view addSubview:scroller];
    
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds)-100);
    spinner.tag = 12;
    
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    
    [self call_data_service];
    
   
    /* Code END */
    
}

-(void)single_box:(NSString*)image charity_name:(NSString*)name description:(NSString*)desc country_code:(NSString*)country_code currency:(NSString*)currency raw_data:(NSDictionary*)raw_array odd:(BOOL)odd{
    
   
    
    LayoutBoxHelper *box = [LayoutBoxHelper toptitle_image_bottomtext:image top_text:name bottom_text:desc country_code:country_code currency:currency size:self.view.size odd:odd];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    box.onTap = ^{
        
        [self charity_detail:raw_array];
        
        
    };
}




-(void)pagination_btn{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 1.5, self.view.size.width/2, 100)];
    box.backgroundColor = [[Appearances instance] view_controller_background_color];
    
    UIView *viewBottom = [[UIView alloc] initWithFrame:CGRectMake(0, 1.5, box.size.width, box.size.height)];
    viewBottom.backgroundColor = [UIColor blackColor];
    viewBottom.alpha = 0.7;
    [box addSubview:viewBottom];
    
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewBottom.size.width, viewBottom.size.height)];
    top_text_label.textAlignment = NSTextAlignmentCenter;
    top_text_label.text = @"NEXT";
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    top_text_label.textColor = [UIColor whiteColor];
    [viewBottom addSubview:top_text_label];
    
    lable_helper = top_text_label;
    
    [scroller.boxes addObject:box];
    
    
    box.onTap = ^{
        
        lable_helper.text = @"LOADING";
        
        currentPage = currentPage+1;
        
        NSArray *listItems = [query componentsSeparatedByString:@"&"];
        NSMutableArray *arr = [[NSMutableArray alloc] init];;
        if(listItems.count > 0)
        {
            for(int i=1;i<listItems.count;i++)
            {
                [arr addObject:[listItems objectAtIndex:i]];
            }
            NSString *combined = [arr componentsJoinedByString:@"&"];
            query = [NSString stringWithFormat:@"page=%d&%@",currentPage,combined];
        }
        else
        {
            query = [NSString stringWithFormat:@"page=%d",currentPage];
        }
        
        [self call_data_service_pagination];
    };
    
}


-(void)charity_detail:(NSDictionary*)raw{
    
   
    CharityDetailViewContoller *popin = [[CharityDetailViewContoller alloc] init];
    [popin set_data:raw];
    [popin setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
    
   
    [popin setPopinOptions:BKTPopinDisableAutoDismiss];
    

    [popin setPopinTransitionDirection:BKTPopinTransitionStyleSlide];
    
    
    [[[ViewControllerHelper instance] get_tabbar_controller] presentPopinController:popin animated:YES completion:^{
        NSLog(@"Popin presented !");
    }];
    
}

-(void)set_query:(NSString*)str{
    query = str;
}

-(void)call_data_service{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        data = [[DataService instance] get_charities_list:[NSString stringWithFormat:@"limit=%@&%@",ITEM_PER_PAGE,query]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            //Send to UI
            
            
            NSArray *charities = [data objectForKey:@"charities"];
            
            for(int i=0;i<charities.count;i++)
            {
                NSDictionary *object =[charities objectAtIndex:i];
                
                NSString *logo = [object objectForKey:@"logo_url"];
                NSString *name = [object objectForKey:@"name"];
                NSString *description = [object objectForKey:@"description"];
                NSString *country_code = [object objectForKey:@"country_code"];
                NSString *currency = [[object objectForKey:@"currency"] objectForKey:@"iso_code"];
                
                if ([logo isEqual:[NSNull null]])
                {
                    logo = @"";
                }
                
                if ([name isEqual:[NSNull null]])
                {
                    name = @"";
                }
                
                if ([description isEqual:[NSNull null]])
                {
                    description = @"";
                }
                
                if ([country_code isEqual:[NSNull null]])
                {
                    country_code = @"";
                }
                
                if ([currency isEqual:[NSNull null]])
                {
                    currency = @"";
                }
                
               
                
                if (i & 1) {
                [self single_box:logo charity_name:name description:description country_code:country_code currency:currency raw_data:object odd:true];
                }
                else
                {
                   [self single_box:logo charity_name:name description:description country_code:country_code currency:currency raw_data:object  odd:false];
                }
                
                
            }
            
            
            
            NSDictionary *meta = [data objectForKey:@"meta"];
            currentPage = [[meta objectForKey:@"current_page"] intValue];
            totalPage = [[meta objectForKey:@"total_pages"] intValue];
            
            if(totalPage > currentPage)
            {
                [self pagination_btn];
            }
            
            [scroller layoutWithDuration:0.3 completion:nil];
            [spinner stopAnimating];
        });
        
    });
    
    
}


-(void)call_data_service_pagination{
    
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        data = [[DataService instance] get_charities_list:[NSString stringWithFormat:@"limit=%@&%@",ITEM_PER_PAGE,query]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            //Send to UI
            [scroller.boxes removeLastObject];
            
            NSArray *charities = [data objectForKey:@"charities"];
            
            for(int i=0;i<charities.count;i++)
            {
                NSDictionary *object =[charities objectAtIndex:i];
                
                NSString *logo = [object objectForKey:@"logo_url"];
                NSString *name = [object objectForKey:@"name"];
                NSString *description = [object objectForKey:@"description"];
                NSString *country_code = [object objectForKey:@"country_code"];
                NSString *currency = [[object objectForKey:@"currency"] objectForKey:@"iso_code"];
                
                if ([logo isEqual:[NSNull null]])
                {
                    logo = @"";
                }
                
                if ([name isEqual:[NSNull null]])
                {
                    name = @"";
                }
                
                if ([description isEqual:[NSNull null]])
                {
                    description = @"";
                }
                
                if ([country_code isEqual:[NSNull null]])
                {
                    country_code = @"";
                }
                
                if ([currency isEqual:[NSNull null]])
                {
                    currency = @"";
                }
                
                
                
                if (i & 1) {
                    [self single_box:logo charity_name:name description:description country_code:country_code currency:currency raw_data:object odd:true];
                }
                else
                {
                    [self single_box:logo charity_name:name description:description country_code:country_code currency:currency raw_data:object  odd:false];
                }
                
                
            }
            
            
            
            NSDictionary *meta = [data objectForKey:@"meta"];
            currentPage = [[meta objectForKey:@"current_page"] intValue];
            totalPage = [[meta objectForKey:@"total_pages"] intValue];
            
            if(totalPage > currentPage)
            {
                [self pagination_btn];
                lable_helper.text = @"NEXT";
            }
            
            [scroller layoutWithDuration:0.3 completion:nil];
            [spinner stopAnimating];
        });
        
    });
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
