//
//  GlobalSettings.h
//  donateapp
//
//  Created by Dead Mac on 31/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#ifndef GlobalSettings_h
#define GlobalSettings_h

#define MAIN_URL_EVERYDAYHERO @"https://everydayhero.com/api/v2/" // to get charities data

#define BACKEND_MAIN_URL @"http://donateapp.jobinic.com/" // backend url
#define BACKEND_API_KEY @"03e236b404ae7518d4feab8a2be4dc18" // backend API KEY

#define ITEM_PER_PAGE @"19"
#define DEFAULT_CURRENCY @"USD"

#endif /* GlobalSettings_h */
