//
//  PaymentMethodViewController.h
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu/CAPSPageMenu.h"
#import "CreditCardViewController.h"
#import "BankTransferViewController.h"
#import "ComingSoonViewController.h"
#import "PreviousCCPaymentController.h"
@interface PaymentMethodViewController : UIViewController

@end
