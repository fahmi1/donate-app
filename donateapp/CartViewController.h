//
//  CartViewController.h
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutBoxHelper.h"
#import "PaymentMethodViewController.h"
#import "SCLAlertView.h"
@interface CartViewController : UIViewController
{
    MGScrollView *scroller;
    UILabel *label_instruction;
    UITextField *email;
    UITextField *password;
    SCLAlertView *alertLogin;
}
-(void)refresh_content;
@end
