//
//  CheckOutDataService.m
//  donateapp
//
//  Created by Dead Mac on 04/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "CheckOutDataService.h"

@implementation CheckOutDataService
+ (CheckOutDataService *) instance {
    static CheckOutDataService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(NSDictionary*)pay_now_previous_credit_card{
    
    NSString *user_hash_id = [[[UserDataService instance] user_data] objectForKey:@"hash_id"];
    NSString *billing_name = [[[UserDataService instance] user_data] objectForKey:@"name"];
    NSString *billing_email = [[[UserDataService instance] user_data] objectForKey:@"email"];
    NSString *total = [NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]];
    
    
    NSMutableArray *cart = [[CartHelper instance] get_cart];
    
    
    /* START CART ITEM */
    SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    
    
    NSMutableArray *new_cart = [[NSMutableArray alloc] init];
    
    for(int i=0;i<cart.count;i++)
    {
        NSDictionary *cart_data = [cart objectAtIndex:i];
        
        NSDictionary *raw_data = [cart_data objectForKey:@"data"];
        NSString *name = [raw_data objectForKey:@"name"];
        NSString *item_iddasd = [raw_data objectForKey:@"id"];
        NSString *country_code = [raw_data objectForKey:@"country_code"];
        NSString *logo_url = [raw_data objectForKey:@"logo_url"];
        NSString *desc = [raw_data objectForKey:@"description"];
        NSString *value = [cart_data objectForKey:@"value"];
        NSString *frequency = [cart_data objectForKey:@"frequency"];
        NSString *currency = [[raw_data objectForKey:@"currency"] objectForKey:@"iso_code"];
        
        
        
        NSDictionary* dict = @{@"item_id":item_iddasd,
                               @"item_image_url":logo_url,
                               @"item_name":name,
                               @"item_description":desc,
                               @"item_country":country_code,
                               @"item_currency":currency,
                               @"item_frequency":frequency,
                               @"item_value":value
                               };
        [new_cart addObject:dict];
    }
    
    
    
    NSString *jsonString = [writer stringWithObject:new_cart error:nil];
    
    
    NSString* encodedJson = [jsonString stringByAddingPercentEscapesUsingEncoding:
                             NSUTF8StringEncoding];
    
    /* END CART ITEM */
    /* START Payment Data  */
    NSDictionary* dict = @{@"card_number":@""
                           };
    SBJsonWriter *writer2 = [[SBJsonWriter alloc] init];
    NSString *payment_data_json = [writer2 stringWithObject:dict error:nil];
    NSString* encoded_payment_data_json = [payment_data_json stringByAddingPercentEscapesUsingEncoding:
                                           NSUTF8StringEncoding];
    
    
    /* END Payment Data  */
    
    return [self pay_now_prev:user_hash_id billing_name:billing_name billing_email:billing_email currency:[[Appearances instance] default_currency]  total:total payment_method_id:@"credit_card" items_json:encodedJson payment_data_json:encoded_payment_data_json];
    
}



-(NSDictionary*)pay_now_credit_card:(NSString*)payment_method credit_card:(NSString*)cc cc_expiry:(NSString*)expiry cvv:(NSString*)cvv{
    
    NSString *user_hash_id = [[[UserDataService instance] user_data] objectForKey:@"hash_id"];
    NSString *billing_name = [[[UserDataService instance] user_data] objectForKey:@"name"];
    NSString *billing_email = [[[UserDataService instance] user_data] objectForKey:@"email"];
    NSString *total = [NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]];
    
    
    NSMutableArray *cart = [[CartHelper instance] get_cart];
   
    
    /* START CART ITEM */
     SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    
    
    NSMutableArray *new_cart = [[NSMutableArray alloc] init];
    
    for(int i=0;i<cart.count;i++)
    {
        NSDictionary *cart_data = [cart objectAtIndex:i];
        
        NSDictionary *raw_data = [cart_data objectForKey:@"data"];
        NSString *name = [raw_data objectForKey:@"name"];
        NSString *item_iddasd = [raw_data objectForKey:@"id"];
         NSString *country_code = [raw_data objectForKey:@"country_code"];
         NSString *logo_url = [raw_data objectForKey:@"logo_url"];
          NSString *desc = [raw_data objectForKey:@"description"];
        NSString *value = [cart_data objectForKey:@"value"];
        NSString *frequency = [cart_data objectForKey:@"frequency"];
         NSString *currency = [[raw_data objectForKey:@"currency"] objectForKey:@"iso_code"];
        
        
        
        NSDictionary* dict = @{@"item_id":item_iddasd,
                               @"item_image_url":logo_url,
                               @"item_name":name,
                               @"item_description":desc,
                               @"item_country":country_code,
                               @"item_currency":currency,
                               @"item_frequency":frequency,
                               @"item_value":value
                               };
        [new_cart addObject:dict];
    }
    
   
    
    NSString *jsonString = [writer stringWithObject:new_cart error:nil];
    

    NSString* encodedJson = [jsonString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
     /* END CART ITEM */
    /* START Payment Data  */
    NSDictionary* dict = @{@"card_number":[cc stringByReplacingOccurrencesOfString:@" " withString:@""],
                           @"expiry_date":expiry,
                           @"cvv":cvv,
                           @"name":billing_name,
                           @"email":billing_email,
                           @"total":total
                           };
     SBJsonWriter *writer2 = [[SBJsonWriter alloc] init];
     NSString *payment_data_json = [writer2 stringWithObject:dict error:nil];
    NSString* encoded_payment_data_json = [payment_data_json stringByAddingPercentEscapesUsingEncoding:
                             NSUTF8StringEncoding];
    
    
     /* END Payment Data  */
    
   return [self pay_now_send:user_hash_id billing_name:billing_name billing_email:billing_email currency:[[Appearances instance] default_currency]  total:total payment_method_id:payment_method items_json:encodedJson payment_data_json:encoded_payment_data_json];
    
}


-(NSDictionary*)pay_now_bank_transfer:(NSString*)payment_method{
    
    NSString *user_hash_id = [[[UserDataService instance] user_data] objectForKey:@"hash_id"];
    NSString *billing_name = [[[UserDataService instance] user_data] objectForKey:@"name"];
    NSString *billing_email = [[[UserDataService instance] user_data] objectForKey:@"email"];
    NSString *total = [NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]];
    
    
    NSMutableArray *cart = [[CartHelper instance] get_cart];
    
    
    /* START CART ITEM */
    SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    
    
    NSMutableArray *new_cart = [[NSMutableArray alloc] init];
    
    for(int i=0;i<cart.count;i++)
    {
        NSDictionary *cart_data = [cart objectAtIndex:i];
        
        NSDictionary *raw_data = [cart_data objectForKey:@"data"];
        NSString *name = [raw_data objectForKey:@"name"];
        NSString *item_iddasd = [raw_data objectForKey:@"id"];
        NSString *country_code = [raw_data objectForKey:@"country_code"];
        NSString *logo_url = [raw_data objectForKey:@"logo_url"];
        NSString *desc = [raw_data objectForKey:@"description"];
        NSString *value = [cart_data objectForKey:@"value"];
        NSString *frequency = [cart_data objectForKey:@"frequency"];
        NSString *currency = [[raw_data objectForKey:@"currency"] objectForKey:@"iso_code"];
        
        
        
        NSDictionary* dict = @{@"item_id":item_iddasd,
                               @"item_image_url":logo_url,
                               @"item_name":name,
                               @"item_description":desc,
                               @"item_country":country_code,
                               @"item_currency":currency,
                               @"item_frequency":frequency,
                               @"item_value":value
                               };
        [new_cart addObject:dict];
    }
    
    
    
    NSString *jsonString = [writer stringWithObject:new_cart error:nil];
    
    
    NSString* encodedJson = [jsonString stringByAddingPercentEscapesUsingEncoding:
                             NSUTF8StringEncoding];
    
    /* END CART ITEM */
    
    /* START Payment Data  */
    NSDictionary* dict = @{
                           @"mock":@"mock"
                           };
    SBJsonWriter *writer2 = [[SBJsonWriter alloc] init];
    NSString *payment_data_json = [writer2 stringWithObject:dict error:nil];
    NSString* encoded_payment_data_json = [payment_data_json stringByAddingPercentEscapesUsingEncoding:
                                           NSUTF8StringEncoding];
    
    
    /* END Payment Data  */
    
    return [self pay_now_send:user_hash_id billing_name:billing_name billing_email:billing_email currency:[[Appearances instance] default_currency]  total:total payment_method_id:payment_method items_json:encodedJson payment_data_json:encoded_payment_data_json];
    
}


-(NSDictionary*)pay_now_send:(NSString*)user_hash_id billing_name:(NSString*)billing_name billing_email:(NSString*)billing_email  currency:(NSString*)currency total:(NSString*)total payment_method_id:(NSString*)payment_method_id items_json:(NSString*)items_json payment_data_json:(NSString*)payment_data_json{
    
   
    
    NSString *post =[NSString stringWithFormat:@"billing_name=%@&billing_email=%@&user_hash_id=%@&currency=%@&total=%@&payment_method_id=%@&items_json=%@&payment_data_json=%@",billing_name,billing_email,user_hash_id,currency,total,payment_method_id,items_json,payment_data_json];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/pay_now/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",rawJson);
    
    NSDictionary *data = [rawJson JSONValue];
    
    
    
    return data;
}


-(NSDictionary*)pay_now_prev:(NSString*)user_hash_id billing_name:(NSString*)billing_name billing_email:(NSString*)billing_email  currency:(NSString*)currency total:(NSString*)total payment_method_id:(NSString*)payment_method_id items_json:(NSString*)items_json payment_data_json:(NSString*)payment_data_json{
    
    
    
    NSString *post =[NSString stringWithFormat:@"billing_name=%@&billing_email=%@&user_hash_id=%@&currency=%@&total=%@&payment_method_id=%@&items_json=%@&payment_data_json=%@",billing_name,billing_email,user_hash_id,currency,total,payment_method_id,items_json,payment_data_json];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/pay_now_previous/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",rawJson);
    
    NSDictionary *data = [rawJson JSONValue];
    
    
    
    return data;
}



@end
