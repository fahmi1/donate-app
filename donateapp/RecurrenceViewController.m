//
//  RecurrenceViewController.m
//  donateapp
//
//  Created by Dead Mac on 05/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "RecurrenceViewController.h"

@implementation RecurrenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling*/
    
    
    /* Start Code */
    
    //Register to global
    [[ViewControllerHelper instance] set_recur_controller:self];
    
    label_instruction = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, self.view.size.height-100)];
    label_instruction.text = NSLocalizedString(@"RECURRENCE IS EMPTY", nil);
    label_instruction.textAlignment = NSTextAlignmentCenter;
    label_instruction.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:16];
    label_instruction.textColor = [UIColor lightGrayColor];
    label_instruction.hidden = YES;
    [self.view addSubview:label_instruction];
    
    
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    scroller.bottomPadding = 100;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame)+30, 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    
    [self.view addSubview:scroller];
    
    
    
    [self content];
    
    /* End Code */
}

-(void)content{
    
    NSArray *array = [[UserDataService instance].user_data objectForKey:@"recurrence"];
    
    for(int i=0;i<array.count;i++)
    {
        NSDictionary *rec_data = [array objectAtIndex:i];

        NSString *freq_word = [[[Appearances instance] frequecy_variable] objectAtIndex:[[rec_data objectForKey:@"frequency"] intValue]];
        
        [self single_box:[rec_data objectForKey:@"item_name"] data:rec_data frequency:freq_word value:[rec_data objectForKey:@"value"]];
    }
    
    if(array.count == 0)
    {
        label_instruction.hidden = NO;
    }
    else
    {
        label_instruction.hidden = YES;
    }
    
    [scroller layoutWithDuration:0.3 completion:nil];
}

-(void)refresh_content{
    [scroller.boxes removeAllObjects];
    [self content];
}

-(void)single_box:(NSString*)item_name data:(NSDictionary*)data frequency:(NSString*)frequency value:(NSString*)value{
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_layout:self.view.size charity_name:item_name value:value frequency:frequency];
    
    
    [scroller.boxes addObject:box];
    
    
    
    
    
    
}

-(void)delete_reccur:(UserDataTapGestureRecognizer*)tap{
    
    
    
    //Refresh Cart
    [self refresh_content];
    [scroller layoutWithDuration:0.3 completion:nil];
    
    
}


@end
