//
//  ViewController.m
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation ViewController

-(id)init{
    self = [super init];
    if(self) {
       
        
         [self setTitle:NSLocalizedString(@"CHARITIES", nil)];
         
        
    }
    
    return self;
}
- (void)viewDidAppear:(BOOL)animated {
    [[ViewControllerHelper instance] get_tabbar_controller].navigationItem.title=NSLocalizedString(@"CHARITIES", nil);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
     /* END View Controller Styling */
    
    /* Code START Here */
    
    [self addHorizontalMenu];
    
    /* Code END */
    
}

-(void)addHorizontalMenu
{
    
    NSMutableArray *controllerArray = [NSMutableArray array];
    
    ListItemController *controller = [[ListItemController alloc] init];
    [controller set_query:@"page=1" ];
    controller.title = NSLocalizedString(@"BROWSE", nil);
    [controllerArray addObject:controller];
    
    
    ListItemController *controller2 = [[ListItemController alloc] init];
    [controller2 set_query:@"page=1&country_code=uk" ];
    controller2.title = NSLocalizedString(@"UK", nil);
    [controllerArray addObject:controller2];
    
    
    ListItemController *controller3 = [[ListItemController alloc] init];
    [controller3 set_query:@"page=1&country_code=au" ];
    controller3.title = NSLocalizedString(@"AUS", nil);
    [controllerArray addObject:controller3];
    
    
    ListItemController *controller5 = [[ListItemController alloc] init];
    [controller5 set_query:@"page=1&country_code=nz" ];
    controller5.title = NSLocalizedString(@"NZ", nil);
    [controllerArray addObject:controller5];
    
    
    ListItemController *controller4 = [[ListItemController alloc] init];
    [controller4 set_query:@"page=1&search=animal" ];
    controller4.title = NSLocalizedString(@"ANIMAL", nil);
    [controllerArray addObject:controller4];
    
    
    ListItemController *controller8 = [[ListItemController alloc] init];
    [controller8 set_query:@"page=1&search=health" ];
    controller8.title = NSLocalizedString(@"HEALTH", nil);
    [controllerArray addObject:controller8];
    
    
    ListItemController *controller9 = [[ListItemController alloc] init];
    [controller9 set_query:@"page=1&search=education" ];
    controller9.title = NSLocalizedString(@"EDUCATION", nil);
    [controllerArray addObject:controller9];
    
    ListItemController *controller10 = [[ListItemController alloc] init];
    [controller10 set_query:@"page=1&search=women" ];
    controller10.title = NSLocalizedString(@"WOMEN", nil);
    [controllerArray addObject:controller10];
    
    // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
    // Example:

    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor redColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:[[Appearances instance] font_family_regular] size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(90.0),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    // Initialize page menu with controller array, frame, and optional parameters
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 63, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    
    
}


- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
