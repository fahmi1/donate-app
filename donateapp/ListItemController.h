//
//  ListItemController.h
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutBoxHelper.h"
#import "CharityDetailViewContoller.h"
@interface ListItemController : UIViewController
{
    MGScrollView *scroller;
    NSString *query;
    
    NSDictionary *data;
    UIActivityIndicatorView *spinner;
    CGPoint initialContentOffset;
    UILabel *lable_helper;
    int totalPage;
    int currentPage;
    float fixedHeight;
    BOOL processing;
}
-(void)set_query:(NSString*)str;
@end
