//
//  UserDataTapGestureRecognizer.h
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataTapGestureRecognizer : UITapGestureRecognizer
@property (nonatomic, strong) id userData;
@property (nonatomic, strong) UILabel *currentLabel;
@property (nonatomic, strong) id openPopOver;
@end
