//
//  CheckOutDataService.h
//  donateapp
//
//  Created by Dead Mac on 04/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckOutDataService : NSObject
+ (CheckOutDataService *) instance;
-(NSDictionary*)pay_now_previous_credit_card;
-(NSDictionary*)pay_now_credit_card:(NSString*)payment_method credit_card:(NSString*)cc cc_expiry:(NSString*)expiry cvv:(NSString*)cvv;
-(NSDictionary*)pay_now_bank_transfer:(NSString*)payment_method;
@end
