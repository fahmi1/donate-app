//
//  WebViewController.h
//  donateapp
//
//  Created by Dead Mac on 04/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
{
    NSString *url;
    UIWebView *webview;
}
@property(nonatomic,retain)UIWebView *webview;
-(void)set_url:(NSString*)urlstring;
@end
