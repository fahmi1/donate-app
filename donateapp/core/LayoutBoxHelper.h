//
//  LayoutBoxHelper.h
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGBox.h"
#import "SDWebImageManager.h"

@interface LayoutBoxHelper : MGBox
+(LayoutBoxHelper *)cart_layout:(CGSize)size charity_name:(NSString*)name value:(NSString*)value frequency:(NSString*)frequency;
+(LayoutBoxHelper *)cart_sum_layout:(CGSize)size value:(NSString*)value;
+(LayoutBoxHelper *)toptitle_image_bottomtext:(NSString*)featuredImage top_text:(NSString*)top_text bottom_text:(NSString*)bottom_text country_code:(NSString*)country_code currency:(NSString*)currency size:(CGSize)size odd:(BOOL)odd;
+(LayoutBoxHelper *)user_display_data:(CGSize)size fa_icon:(NSString*)font_awesome label:(NSString*)label value:(NSString*)value;
+(LayoutBoxHelper *)text_center_box:(CGSize)size  text:(NSString*)text;
@end
