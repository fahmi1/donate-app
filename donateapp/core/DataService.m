//
//  NSObject_DataService_m.h
//  donator
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//



#import "DataService.h"

@implementation DataService
@synthesize get_charities_data;
+ (DataService *) instance {
    static DataService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}



-(NSDictionary*)get_charities_list:(NSString*)query{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/charities.json?%@",MAIN_URL_EVERYDAYHERO,query];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSData *sa = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString* rawJson = [[NSString alloc] initWithData:sa encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    return data;
}


-(NSDictionary*)search_charities_list:(NSString*)query{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/search/charities?%@",MAIN_URL_EVERYDAYHERO,query];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSData *sa = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString* rawJson = [[NSString alloc] initWithData:sa encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    return data;
}


-(NSDictionary*)single_charity:(NSString*)uid{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/charities/%@.json",MAIN_URL_EVERYDAYHERO,uid];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSData *sa = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString* rawJson = [[NSString alloc] initWithData:sa encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    return data;
}




@end
