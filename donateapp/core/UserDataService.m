//
//  UserDataService.m
//  donateapp
//
//  Created by Dead Mac on 02/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "UserDataService.h"

@implementation UserDataService
@synthesize user_data;
+ (UserDataService *) instance {
    static UserDataService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(void)set_user_data:(NSDictionary*)data{
    user_data = data;
    
    //Save user data into device
    [[FileSaveHelper instance] saveNSMutableDictionaryAsFile:[user_data mutableCopy] initWithFileName:@"user_data"];
    
    
    
}

-(BOOL)check_user_if_already_logged{
   
    if([[FileSaveHelper instance] checkFileIfExists:@"user_data"] == true)
    {
        //then retriveback the data and register in global
        user_data = [[[FileSaveHelper instance] getNSMutableDictionaryFile:@"user_data"] mutableCopy];
        return true;
    }
    else
    {
        return false;
    }
    
    
    
}

-(NSString*)get_user_image_url{
    
    return [NSString stringWithFormat:@"%@/uploaded/%@",BACKEND_MAIN_URL,[user_data objectForKey:@"profile_image"]];
}


-(NSDictionary*)register_user:(NSString*)name email:(NSString*)email password:(NSString*)password{
    
    //NSString *deviceType = [UIDevice currentDevice].model;
    
    NSString *post =[NSString stringWithFormat:@"name=%@&email=%@&password=%@",name,email,password];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/register/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    
    return data;
}




-(NSDictionary*)login:(NSString*)email password:(NSString*)password{
    
   
    
    NSString *post =[NSString stringWithFormat:@"email=%@&password=%@",email,password];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/login/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    
    return data;
}



-(NSDictionary*)login_fb:(NSString*)fb_id fb_email:(NSString*)fb_email fb_name:(NSString*)fb_name{
    
  
    
    NSString *post =[NSString stringWithFormat:@"fb_id=%@&fb_name=%@&fb_email=%@",fb_id,fb_name,fb_email];
    
   
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/login_fb/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    
    return data;
}


-(NSDictionary*)logout:(NSString*)user_hash_id{
    
    
    
    NSString *post =[NSString stringWithFormat:@"user_hash_id=%@",user_hash_id];
    
    
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/logout/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    
    return data;
}

-(void)refresh_user_data{
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
      NSDictionary *new_user_data = [self get_user_by_hash_id:[user_data objectForKey:@"hash_id"]];
        //Set new global data
        [self set_user_data:[new_user_data objectForKey:@"response"]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [[[ViewControllerHelper instance] get_user_controller] refresh_content];
            [[[ViewControllerHelper instance] get_donation_controller] refresh_content];
        });
        
    });
    
    
    
    
    
}

-(NSDictionary*)get_user_by_hash_id:(NSString*)hash_id{
    
    //NSString *deviceType = [UIDevice currentDevice].model;
    
    NSString *post =[NSString stringWithFormat:@"user_hash_id=%@",hash_id];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/member/?api_key=%@",BACKEND_MAIN_URL,BACKEND_API_KEY]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    NSString *rawJson = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = [rawJson JSONValue];
    
    
    
    return data;
}




@end
