//
//  CartHelper.h
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartHelper : NSObject
{
    NSMutableArray *cart;
}
+ (CartHelper *) instance;
-(void)initCart; //will add when add start
-(void)addToCart:(NSDictionary*)data value:(NSString*)value frequency:(NSString*)frequency;
-(void)delete_item:(int)object_id;
-(NSMutableArray*)get_cart;
-(int)count_total;
-(void)reset_cart;
@end
