//
//  Appearances.m
//  donator
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "Appearances.h"

@implementation Appearances

+ (Appearances *) instance {
    static Appearances *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(void)global_settings{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UITabBar appearance] setTintColor:[self nav_bar_color]];
    [[UINavigationBar appearance] setTintColor:[self nav_bar_font_color]];
    
     [[ViewControllerHelper instance] get_main_navigation].navigationBar.barTintColor = [self nav_bar_color];
    
    [[[ViewControllerHelper instance] get_tabbar_controller].navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[self nav_bar_font_color]}];
    
    //Will init the cart
    [[CartHelper instance] initCart];
    
    
    
    
}

-(UIColor*)nav_bar_color{
    
    return [UIColor redColor];
}

-(UIColor*)nav_bar_font_color{
    
    return [UIColor whiteColor];
}

-(UIColor*)tab_bar_selected_color{
    
    return [UIColor redColor];
}

-(UIColor*)view_controller_background_color{
    
    return [UIColor whiteColor];
}

-(NSString*)font_family_regular{
    
    return @"Helvetica";
}

-(NSString*)font_family_bold{
    
    return @"Helvetica-Bold";
}

-(NSString*)default_currency{
    
    return DEFAULT_CURRENCY;
}

-(NSArray*)frequecy_variable{
    
    NSArray *array = @[NSLocalizedString(@"ONCE OFF", nil),NSLocalizedString(@"WEEKLY", nil),NSLocalizedString(@"FORTNIGHTLY", nil), NSLocalizedString(@"MONTHLY", nil)];
    
    return array;
}

-(NSArray*)donation_template{
    
    NSArray *value_array = @[ @"1",@"5",@"10", @"30",@"50",@"100",@"200",@"300",@"500"]; //Value Only
    
    
    return value_array;
}

@end
