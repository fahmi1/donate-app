//
//  LayoutBoxHelper.m
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "LayoutBoxHelper.h"

@implementation LayoutBoxHelper
- (void)setup {
    
    
    
    // background
    self.backgroundColor = [UIColor whiteColor];
    //self.layer.borderWidth = 1;
    //self.layer.borderColor = [UIColor whiteColor].CGColor;
}


+(LayoutBoxHelper *)cart_layout:(CGSize)size charity_name:(NSString*)name value:(NSString*)value frequency:(NSString*)frequency{
    LayoutBoxHelper *box = [LayoutBoxHelper boxWithSize:CGSizeMake(size.width,58)];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 53)];
    view.backgroundColor = [UIColor colorWithWhite:0.96 alpha:1.0];
    view.userInteractionEnabled = YES;
    [box addSubview:view];
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width*0.65, 20)];
    top_text_label.text = [name uppercaseString];
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    top_text_label.textColor = [UIColor blackColor];
    top_text_label.userInteractionEnabled = YES;
    [top_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    
    top_text_label.numberOfLines = 0;
    [view addSubview:top_text_label];
    
    UILabel *bottom_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, box.width*0.65, 20)];
    bottom_text_label.text = frequency;
    bottom_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:12];
    bottom_text_label.textColor = [UIColor blackColor];
    bottom_text_label.userInteractionEnabled = YES;
    [bottom_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    
    bottom_text_label.numberOfLines = 0;
    [view addSubview:bottom_text_label];
    
    
    UILabel *right_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width-20, 20)];
    right_text_label.text = [NSString stringWithFormat:@"%@ %@",[[Appearances instance] default_currency],value];
    right_text_label.textAlignment = NSTextAlignmentRight;
    right_text_label.userInteractionEnabled = YES;
    right_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    right_text_label.textColor = [UIColor blackColor];
    [view addSubview:right_text_label];
    
    
    return box;
}


+(LayoutBoxHelper *)cart_sum_layout:(CGSize)size value:(NSString*)value{
    LayoutBoxHelper *box = [LayoutBoxHelper boxWithSize:CGSizeMake(size.width,45)];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 40)];
    view.backgroundColor = [UIColor colorWithRed:0.6 green:0.8 blue:0.0 alpha:1.0];;
    [box addSubview:view];
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width*0.65, 20)];
    top_text_label.text = NSLocalizedString(@"TOTAL", nil);
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    top_text_label.textColor = [UIColor whiteColor];
    [top_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    
    top_text_label.numberOfLines = 0;
    [view addSubview:top_text_label];
    
    
    UILabel *right_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width-20, 20)];
    right_text_label.text = [NSString stringWithFormat:@"%@ %@",[[Appearances instance] default_currency],value];
    right_text_label.textAlignment = NSTextAlignmentRight;
    right_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    right_text_label.textColor = [UIColor whiteColor];
    [view addSubview:right_text_label];
    box.userInteractionEnabled = YES;
    return box;
}


+(LayoutBoxHelper *)text_center_box:(CGSize)size  text:(NSString*)text{
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper boxWithSize:CGSizeMake(size.width/2+2,100)];
    
    
    UIView *viewBottom = [[UIView alloc] initWithFrame:CGRectMake(0, 1.5, box.size.width, box.size.height)];
    viewBottom.backgroundColor = [UIColor blackColor];
    viewBottom.alpha = 0.7;
    [box addSubview:viewBottom];
    
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewBottom.size.width, viewBottom.size.height)];
    top_text_label.textAlignment = NSTextAlignmentCenter;
    top_text_label.text = [text uppercaseString];
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    top_text_label.textColor = [UIColor whiteColor];
    [viewBottom addSubview:top_text_label];
    
    
    
    return box;
}




//BOX with image top title and bottom text

+(LayoutBoxHelper *)toptitle_image_bottomtext:(NSString*)featuredImage top_text:(NSString*)top_text bottom_text:(NSString*)bottom_text country_code:(NSString*)country_code currency:(NSString*)currency size:(CGSize)size odd:(BOOL)odd{
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper boxWithSize:CGSizeMake(size.width/2+2,100)];
    
    UIImageView *featuredImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1.5, size.width/2, 100)];
    [featuredImgView setContentMode:UIViewContentModeCenter];
    
    UIImageView *se = featuredImgView;
    
    [featuredImgView setImageWithURL:[NSURL URLWithString:featuredImage]
                    placeholderImage:[UIImage imageNamed:NSLocalizedString(@"image_loading_placeholder", nil)]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                               
                              // se.image = [[ToolHelper instance] imageByScalingAndCroppingForSize:CGSizeMake(100, 100) source:image];
                               se.image = image;
                           }];
    [box addSubview:featuredImgView];
    
    
    CGRect sds;
    if(odd == true)
    {
        sds = CGRectMake(0, 0, featuredImgView.width, featuredImgView.height);
    }
    else
    {
        sds = CGRectMake(0, 0, featuredImgView.width, featuredImgView.height);
    }
    UIView *viewBottom = [[UIView alloc] initWithFrame:sds];
    viewBottom.backgroundColor = [UIColor blackColor];
    viewBottom.alpha = 0.7;
    [featuredImgView addSubview:viewBottom];
   
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, featuredImgView.width-20, 40)];
    top_text_label.text = [top_text uppercaseString];
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:11];
    top_text_label.textColor = [UIColor whiteColor];
    [top_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    
    top_text_label.numberOfLines = 0;
    [viewBottom addSubview:top_text_label];
    
    
    UILabel *bottom_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 33, featuredImgView.width-20, 40)];
    bottom_text_label.text = [[bottom_text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    bottom_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:10];
    bottom_text_label.textColor = [UIColor whiteColor];
    [bottom_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    
    bottom_text_label.numberOfLines = 0;
    [viewBottom addSubview:bottom_text_label];
    
    UIImageView *flag = [[UIImageView alloc] initWithFrame:CGRectMake(7, 77, 20, 10)];
    [flag setContentMode:UIViewContentModeScaleAspectFit];
    flag.image = [UIImage imageNamed:[NSString stringWithFormat:@"assets/flag/%@.png",[country_code uppercaseString]]];
    [viewBottom addSubview:flag];
    
    
    
    UILabel *bottom_text_currency_label = [[UILabel alloc] initWithFrame:CGRectMake(viewBottom.width-30, 77, 30, 10)];
    bottom_text_currency_label.text = [currency uppercaseString];
    bottom_text_currency_label.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:9];
    bottom_text_currency_label.textColor = [UIColor whiteColor];
    
    [viewBottom addSubview:bottom_text_currency_label];
    
    
    return box;
}


+(LayoutBoxHelper *)user_display_data:(CGSize)size fa_icon:(NSString*)font_awesome label:(NSString*)label value:(NSString*)value{
    LayoutBoxHelper *box = [LayoutBoxHelper boxWithSize:CGSizeMake(size.width,45)];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 40)];
    view.backgroundColor = [UIColor colorWithWhite:0.96 alpha:1.0];
    [box addSubview:view];
    
    FAKFontAwesome *icon = [FAKFontAwesome  iconWithIdentifier:font_awesome size:24 error:nil];
    [icon addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor]];
    UIImage *icon_image = [icon imageWithSize:CGSizeMake(24, 24)];
    
    UIImageView *icon_view = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 18, 18)];
    icon_view.image = icon_image;
    [view addSubview:icon_view];
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(37, 10, box.width*0.65, 20)];
    top_text_label.text = label;
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:14];
    top_text_label.textColor = [UIColor darkGrayColor];
    [top_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    
    top_text_label.numberOfLines = 0;
    [view addSubview:top_text_label];
    
    
    UILabel *right_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width-20, 20)];
    right_text_label.text = [NSString stringWithFormat:@"%@",value];
    right_text_label.textAlignment = NSTextAlignmentRight;
    right_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    right_text_label.textColor = [UIColor blackColor];
    [view addSubview:right_text_label];
    box.userInteractionEnabled = YES;
    return box;
}

@end
