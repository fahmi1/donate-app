//
//  DataService.h
//

//

#import <Foundation/Foundation.h>
@interface DataService : NSObject
{
  
    
}
@property(nonatomic,retain) NSDictionary *get_charities_data;
+ (DataService *) instance;
-(NSDictionary*)get_charities_list:(NSString*)query;
-(NSDictionary*)search_charities_list:(NSString*)query;
-(NSDictionary*)single_charity:(NSString*)uid;
@end
