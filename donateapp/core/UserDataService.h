//
//  UserDataService.h
//  donateapp
//
//  Created by Dead Mac on 02/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataService : NSObject
{
    NSDictionary *user_data;
}
@property(nonatomic,retain) NSDictionary *user_data;
+ (UserDataService *) instance;
-(void)refresh_user_data;
-(NSDictionary*)get_user_by_hash_id:(NSString*)hash_id;
-(NSDictionary*)login:(NSString*)email password:(NSString*)password;
-(NSDictionary*)login_fb:(NSString*)fb_id fb_email:(NSString*)fb_email fb_name:(NSString*)fb_name;
-(NSDictionary*)register_user:(NSString*)name email:(NSString*)email password:(NSString*)password;
-(void)set_user_data:(NSDictionary*)data;
-(BOOL)check_user_if_already_logged;
-(NSDictionary*)logout:(NSString*)user_hash_id;
@end
