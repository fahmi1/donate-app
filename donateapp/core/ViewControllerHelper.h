//
//  ViewControllerHelper.h
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProfileController.h"
#import "LoggedViewController.h"
#import "CartViewController.h"
#import "UserProfileViewController.h"
#import "MyDonationViewController.h"
#import "RecurrenceViewController.h"
@interface ViewControllerHelper : NSObject
{
    UINavigationController *main_navigation;
    UITabBarController *current_tab_bar;
     MyProfileController *my_profile_controller;
    LoggedViewController *logged_controller;
     CartViewController *cart_controller;
    UserProfileViewController *user_controller;
     MyDonationViewController *donation_controller;
    RecurrenceViewController *recur_controller;
}
+ (ViewControllerHelper *) instance;
-(void)set_main_navigation:(UINavigationController*)main;
-(UINavigationController*)get_main_navigation;
-(void)set_tabbar_controller:(UITabBarController*)tab;
-(UITabBarController*)get_tabbar_controller;
-(void)set_my_profile_controller:(MyProfileController*)myProfile;
-(MyProfileController*)get_my_profile_controller_controller;
-(void)set_logged_controller:(LoggedViewController*)logged;
-(LoggedViewController*)get_logged_controller;
-(CartViewController*)get_cart_controller;
-(void)set_cart_controller:(CartViewController*)cart;
-(void)set_user_controller:(UserProfileViewController*)user;
-(UserProfileViewController*)get_user_controller;
-(void)set_donation_controller:(MyDonationViewController*)donation;
-(MyDonationViewController*)get_donation_controller;
-(void)set_recur_controller:(RecurrenceViewController*)recur;
-(RecurrenceViewController*)get_recur_controller;
@end
