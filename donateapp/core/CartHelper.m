//
//  CartHelper.m
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "CartHelper.h"

@implementation CartHelper

+ (CartHelper *) instance {
    static CartHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(void)initCart
{
    cart = [[NSMutableArray alloc] init];
}

-(void)reset_cart{
    
    cart = [[NSMutableArray alloc] init];
    [self update_badge];
}


-(void)addToCart:(NSDictionary*)data value:(NSString*)value frequency:(NSString*)frequency{
    
    NSDictionary* cart_single_data = @{@"data":data, @"value":value, @"frequency":frequency};
    
    
    [cart addObject:cart_single_data];
    [self update_badge];
}

-(void)update_badge{
    
   UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
   [[tab.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%lu",(unsigned long)cart.count]];
}

-(int)count_total
{
    int total = 0;
    for(int i=0;i<cart.count;i++)
    {
        NSDictionary *dic = [cart objectAtIndex:i];
        
        int value = [[dic objectForKey:@"value"] intValue];
         total += value;
       
    }
    
    return total;
}

-(void)delete_item:(int)object_id
{
    [cart removeObjectAtIndex:object_id];
    [self update_badge];
}

-(NSMutableArray*)get_cart{
    
    return cart;
    
}

@end
