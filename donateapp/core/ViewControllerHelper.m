//
//  ViewControllerHelper.m
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "ViewControllerHelper.h"

@implementation ViewControllerHelper
+ (ViewControllerHelper *) instance {
    static ViewControllerHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(void)set_main_navigation:(UINavigationController*)main{
    
    main_navigation = main;
}
-(UINavigationController*)get_main_navigation{
    
    return main_navigation;
}

-(void)set_tabbar_controller:(UITabBarController*)tab{
    
    current_tab_bar = tab;
}
-(UITabBarController*)get_tabbar_controller{
    
    return current_tab_bar;
}


-(void)set_my_profile_controller:(MyProfileController*)myProfile{
    
    my_profile_controller = myProfile;
}

-(MyProfileController*)get_my_profile_controller_controller{
    
    return my_profile_controller;
}


-(void)set_logged_controller:(LoggedViewController*)logged{
    
    logged_controller = logged;
}

-(LoggedViewController*)get_logged_controller{
    
    return logged_controller;
}

-(void)set_cart_controller:(CartViewController*)cart{
    
    cart_controller = cart;
}

-(CartViewController*)get_cart_controller{
    
    return cart_controller;
}

-(void)set_user_controller:(UserProfileViewController*)user{
    
    user_controller = user;
}

-(UserProfileViewController*)get_user_controller{
    
    return user_controller;
}

-(void)set_donation_controller:(MyDonationViewController*)donation{
    
    donation_controller = donation;
}

-(MyDonationViewController*)get_donation_controller{
    
    return donation_controller;
}

-(void)set_recur_controller:(RecurrenceViewController*)recur{
    
    recur_controller = recur;
}

-(RecurrenceViewController*)get_recur_controller{
    
    return recur_controller;
}


@end
