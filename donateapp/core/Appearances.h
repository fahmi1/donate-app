//
//  Appearances.h
//  donator
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Appearances : NSObject
+ (Appearances *) instance;
-(void)global_settings;
-(UIColor*)nav_bar_color;
-(UIColor*)nav_bar_font_color;
-(UIColor*)view_controller_background_color;
-(NSString*)font_family_regular;
-(NSString*)font_family_bold;
-(NSArray*)frequecy_variable;
-(NSString*)default_currency;
-(NSArray*)donation_template;
@end
