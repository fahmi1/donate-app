//
//  PaymentMethodViewController.m
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "PaymentMethodViewController.h"
@interface PaymentMethodViewController ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end
@implementation PaymentMethodViewController

-(id)init{
    self = [super init];
    if(self) {
        
        [self setTitle:NSLocalizedString(@"PAYMENT METHOD", nil)];
        
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
       /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* START */
    NSMutableArray *controllerArray = [NSMutableArray array];
   
    NSString *check =[[UserDataService instance].user_data objectForKey:@"credit_card_truncate"];
     NSLog(@"check %@",check);
    //Will add new controller if previous payment was use credit card
    if([check isEqualToString: @""])
    {
        CreditCardViewController *credit_cart = [[CreditCardViewController alloc] init];
        credit_cart.title = NSLocalizedString(@"CREDIT CARD", nil);
        [controllerArray addObject:credit_cart];
        
        BankTransferViewController *bank_transfer = [[BankTransferViewController alloc] init];
        bank_transfer.title = NSLocalizedString(@"BANK TRANSFER", nil);
        [controllerArray addObject:bank_transfer];
        
        ComingSoonViewController *sms_payment = [[ComingSoonViewController alloc] init];
        sms_payment.title = NSLocalizedString(@"SMS PAYMENT", nil);
        [controllerArray addObject:sms_payment];
        
        
        ComingSoonViewController *premium_voice_call = [[ComingSoonViewController alloc] init];
        premium_voice_call.title = NSLocalizedString(@"PREMIUM VOICE CALL", nil);
        [controllerArray addObject:premium_voice_call];
    }
    else if(check == nil)
    {
        CreditCardViewController *credit_cart = [[CreditCardViewController alloc] init];
        credit_cart.title = NSLocalizedString(@"CREDIT CARD", nil);
        [controllerArray addObject:credit_cart];
        
        BankTransferViewController *bank_transfer = [[BankTransferViewController alloc] init];
        bank_transfer.title = NSLocalizedString(@"BANK TRANSFER", nil);
        [controllerArray addObject:bank_transfer];
        
        ComingSoonViewController *sms_payment = [[ComingSoonViewController alloc] init];
        sms_payment.title = NSLocalizedString(@"SMS PAYMENT", nil);
        [controllerArray addObject:sms_payment];
        
        
        ComingSoonViewController *premium_voice_call = [[ComingSoonViewController alloc] init];
        premium_voice_call.title = NSLocalizedString(@"PREMIUM VOICE CALL", nil);
        [controllerArray addObject:premium_voice_call];
    }
    else
    {
    
     PreviousCCPaymentController *previous_controller = [[PreviousCCPaymentController alloc] init];
        previous_controller.title = NSLocalizedString(@"PREVIOUS METHOD", nil);
        [controllerArray addObject:previous_controller];
        
    CreditCardViewController *credit_cart = [[CreditCardViewController alloc] init];
    credit_cart.title = NSLocalizedString(@"CREDIT CARD", nil);
    [controllerArray addObject:credit_cart];
    
    BankTransferViewController *bank_transfer = [[BankTransferViewController alloc] init];
    bank_transfer.title = NSLocalizedString(@"BANK TRANSFER", nil);
    [controllerArray addObject:bank_transfer];
    
    ComingSoonViewController *sms_payment = [[ComingSoonViewController alloc] init];
    sms_payment.title = NSLocalizedString(@"SMS PAYMENT", nil);
    [controllerArray addObject:sms_payment];
    
    
    ComingSoonViewController *premium_voice_call = [[ComingSoonViewController alloc] init];
    premium_voice_call.title = NSLocalizedString(@"PREMIUM VOICE CALL", nil);
    [controllerArray addObject:premium_voice_call];
        
    }
    
    
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor redColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:[[Appearances instance] font_family_regular] size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(150),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    // Initialize page menu with controller array, frame, and optional parameters
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 63, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    
    /* END CODE */
    
}
@end
