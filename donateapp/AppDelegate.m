//
//  AppDelegate.m
//  donateapp
//
//  Created by Dead Mac on 30/12/2015.
//  Copyright © 2015 Fahmi. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "CartViewController.h"
#import "MyProfileController.h"
#import "LoggedViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
   
    
   
    
   self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
    //First View
    ViewController *launchView = [[ViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:launchView];
    
    FAKFontAwesome *starIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-heart" size:24 error:nil];
    UIImage *iconImage = [starIcon imageWithSize:CGSizeMake(24, 24)];
    nav.tabBarItem.image = iconImage;
    //End First View
    
    //Checkout Controller View
    CartViewController *checkout = [[CartViewController alloc] init];
    UINavigationController *checkout_nav = [[UINavigationController alloc] initWithRootViewController:checkout];
    
    FAKFontAwesome *cartIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-shopping-cart" size:24 error:nil];
    UIImage *cart_image = [cartIcon imageWithSize:CGSizeMake(24, 24)];
    checkout_nav.tabBarItem.image = cart_image;
    
    //Checkout Controller View
    
    //My Profile Controller
    MyProfileController *login = [[MyProfileController alloc] init];
    UINavigationController *login_nav = [[UINavigationController alloc] initWithRootViewController:login];
    
    FAKFontAwesome *loginIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
    UIImage *loginIcon_image = [loginIcon imageWithSize:CGSizeMake(24, 24)];
    login_nav.tabBarItem.image = loginIcon_image;
    
    //End MyProfile Controller
    
    
    //Logged view controller
    LoggedViewController *logged = [[LoggedViewController alloc] init];
    UINavigationController *logged_nav = [[UINavigationController alloc] initWithRootViewController:logged];
    
    FAKFontAwesome *logged_Icon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
    UIImage *logged_Icon_image = [logged_Icon imageWithSize:CGSizeMake(24, 24)];
    logged_nav.tabBarItem.image = logged_Icon_image;
    //END logged view Controller
    
    //Tabbar
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    NSMutableArray *localControllersArray = [[NSMutableArray alloc] init];
    [localControllersArray addObject:nav];
    
    //Check user if already login
    if([[UserDataService instance] check_user_if_already_logged] == false)
    {
        //if no, display login
        [localControllersArray addObject:login_nav];
    }
    else
    {
         //if yes, display logged
        [localControllersArray addObject:logged_nav];
    }
    [localControllersArray addObject:checkout_nav];
    
 
    tabBarController.viewControllers = localControllersArray;
    tabBarController.selectedIndex = 0;
    //End Tabbar
    
    
    //Add into global tabbar
    [[ViewControllerHelper  instance] set_tabbar_controller:tabBarController];
    
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:tabBarController];
   
    //Register into global 
    [[ViewControllerHelper instance] set_main_navigation:navi];
    self.window.rootViewController = navi;
    
     [[Appearances instance] global_settings]; //will list all global settings
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}


@end
