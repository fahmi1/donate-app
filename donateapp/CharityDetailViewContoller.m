//
//  CharityDetailViewContoller.m
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "CharityDetailViewContoller.h"

@implementation CharityDetailViewContoller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.frame = CGRectMake(0, 0, self.view.size.width, 360);
   
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* Code START Here */
    
    //init
     frequecy = @"0";
    
    /* Close Btn Start */
    FAKFontAwesome *cross = [FAKFontAwesome  iconWithIdentifier:@"fa-times" size:24 error:nil];
    UIImage *cross_image = [cross imageWithSize:CGSizeMake(24, 24)];
    
    UIButton *close_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [close_btn setImage:cross_image forState:UIControlStateNormal];
    [close_btn addTarget:self action:@selector(close_btn_action) forControlEvents:UIControlEventTouchUpInside];
    [close_btn setFrame:CGRectMake(self.view.size.width-30, 10, 24, 24)];
    [self.view addSubview:close_btn];
    /* Close Btn End */
    
    [self add_content];
    
    /* Code END */
    
}

-(void)close_btn_action
{
    [[[ViewControllerHelper instance] get_tabbar_controller] dismissCurrentPopinControllerAnimated:YES completion:^{
        NSLog(@"Popin dismissed !");
    }];
}

-(void)add_content{
    
    
    NSString *logo = [data objectForKey:@"logo_url"];
    NSString *name = [data objectForKey:@"name"];
    NSString *description = [data objectForKey:@"description"];
    NSString *country_code = [data objectForKey:@"country_code"];
    NSString *currency = [[data objectForKey:@"currency"] objectForKey:@"iso_code"];
    
    if ([logo isEqual:[NSNull null]])
    {
        logo = @"";
    }
    
    if ([name isEqual:[NSNull null]])
    {
        name = @"";
    }
    
    if ([description isEqual:[NSNull null]])
    {
        description = @"";
    }
    
    if ([country_code isEqual:[NSNull null]])
    {
        country_code = @"";
    }
    
    if ([currency isEqual:[NSNull null]])
    {
        currency = @"";
    }
    
    
    UIImageView *featuredImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 70, 70)];
    [featuredImgView setContentMode:UIViewContentModeCenter];
    
    UIImageView *se = featuredImgView;
    
   
    
    [featuredImgView setImageWithURL:[NSURL URLWithString:logo]
                    placeholderImage:[UIImage imageNamed:NSLocalizedString(@"image_loading_placeholder", nil)]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                               
                                se.image = image;
                               
                           }];
    [self.view addSubview:featuredImgView];
    
    UIImageView *flag = [[UIImageView alloc] initWithFrame:CGRectMake(90, 35, 30, 20)];
    [flag setContentMode:UIViewContentModeScaleAspectFit];
    flag.image = [UIImage imageNamed:[NSString stringWithFormat:@"assets/flag/%@.png",[country_code uppercaseString]]];
    [self.view addSubview:flag];
    
    UILabel *bottom_text_currency_label = [[UILabel alloc] initWithFrame:CGRectMake(130, 41, 30, 10)];
    bottom_text_currency_label.text = [currency uppercaseString];
    bottom_text_currency_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:12];
    bottom_text_currency_label.textColor = [UIColor blackColor];
    
    UILabel *top_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, self.view.size.width-20, 40)];
    top_text_label.text = [name uppercaseString];
    top_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:12];
    top_text_label.textColor = [UIColor blackColor];
    [top_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    top_text_label.numberOfLines = 0;
    [self.view addSubview:top_text_label];
    
    UILabel *desc_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 110, self.view.size.width-20, 100)];
    desc_text_label.text = description;
    desc_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:12];
    desc_text_label.textColor = [UIColor blackColor];
    [desc_text_label setLineBreakMode:NSLineBreakByWordWrapping];
    desc_text_label.numberOfLines = 0;
    [self.view addSubview:desc_text_label];
    
    
    
    [self.view addSubview:bottom_text_currency_label];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 260, self.view.width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    line.layer.masksToBounds =NO;
    [self.view addSubview:line];
    
    
    /* START Choose payment mode */
    UILabel *label_instruction = [[UILabel alloc] initWithFrame:CGRectMake(10, 240, self.view.size.width-20, 20)];
    label_instruction.text = NSLocalizedString(@"CHOOSE FREQUECY", nil);
    label_instruction.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:10];
    label_instruction.textColor = [UIColor darkGrayColor];
   
    [self.view addSubview:label_instruction];
    
    
    
    DKScrollingTabController *tabController = [[DKScrollingTabController alloc] init];
    
    tabController.delegate = self;
    tabController.buttonsScrollView.tag = 1;
    [tabController.buttonsScrollView setShowsHorizontalScrollIndicator:NO];
    [self addChildViewController:tabController];
    [tabController didMoveToParentViewController:self];
     tabController.selectedTextColor = [[Appearances instance] nav_bar_color];
    [self.view addSubview:tabController.view];
    tabController.view.frame = CGRectMake(0, 260, self.view.width, 40);
    tabController.buttonPadding = 27;
    
    tabController.selection = [[Appearances instance] frequecy_variable];

   
    /* END  Choose payment mode*/
    
    
     /* Fast donate */
    UILabel *label_instruction_2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 300, self.view.size.width-20, 20)];
    label_instruction_2.text = NSLocalizedString(@"CHOOSE A VALUE TO DONATE", nil);
    label_instruction_2.font = [UIFont fontWithName:[[Appearances instance] font_family_regular] size:10];
    label_instruction_2.textColor = [UIColor darkGrayColor];
    
    [self.view addSubview:label_instruction_2];
    
    
   
    DKScrollingTabController *tabController2 = [[DKScrollingTabController alloc] init];
    
    tabController2.delegate = self;
    
    [tabController2.buttonsScrollView setShowsHorizontalScrollIndicator:NO];
    [self addChildViewController:tabController2];
    [tabController2 didMoveToParentViewController:self];
    [self.view addSubview:tabController2.view];
    tabController2.view.frame = CGRectMake(0, 320, self.view.width, 40);
    tabController2.buttonPadding = 27;
   
    NSMutableArray* list = [[NSMutableArray alloc] init];
    for(int i=0;i<[[Appearances instance] donation_template].count;i++)
    {
        [list addObject:[NSString stringWithFormat:@"%@ %@",[[Appearances instance] default_currency],[[[Appearances instance] donation_template] objectAtIndex:i]]];
    }
    [list addObject:NSLocalizedString(@"CUSTOM", nil)];
    
    tabController2.selection =list;
     [tabController2 selectNone];
    tabController2.selectedTextColor = [[Appearances instance] nav_bar_color];
    /* END Fast donate */
}

#pragma mark - TabControllerDelegate

- (void)ScrollingTabController:(DKScrollingTabController *)controller selection:(NSUInteger)selection {
  
    
   
    if(controller.buttonsScrollView.tag == 1)
    {
        frequecy = [NSString stringWithFormat:@"%lu",(unsigned long)selection];
        NSLog(@"Freq : %@",frequecy);
    }
    else
    {
    
    
        int custom_int =(unsigned)[[Appearances instance] donation_template].count;
        if(custom_int == selection)
        {
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
            
            UITextField *textField = [alert addTextField:NSLocalizedString(@"Enter your value e.g 25", nil)];
            textField.keyboardType = UIKeyboardTypeNumberPad;
            alert.backgroundType = Blur;
            [alert addButton:NSLocalizedString(@"DONATE", nil) actionBlock:^(void) {
                
                if([textField.text isEqualToString:@""])
                {
                    //Not perform anything
                }
                else
                {
                      [alert hideView];
                    [self close_btn_action];
                    
                    
                    /* Add to cart value */
                    [[CartHelper instance] addToCart:data value:textField.text frequency:frequecy];
                    /* END add to cart */
                  
                }
                [textField resignFirstResponder];
                
            }];
            alert.customViewColor = [[Appearances instance] nav_bar_color];
            alert.shouldDismissOnTapOutside = YES;
            [alert showEdit:self title:NSLocalizedString(@"DONATION", nil) subTitle:NSLocalizedString(@"Type your value to donate", nil)closeButtonTitle:NSLocalizedString(@"CANCEL", nil) duration:0.0f];
            
            
            
        }
        else
        {
            /* Add to cart value */
            
            [[CartHelper instance] addToCart:data value:[[[Appearances instance] donation_template] objectAtIndex:selection] frequency:frequecy];
            /* END add to cart */
            [self close_btn_action];
        }
        
    
    }
}

-(void)set_data:(NSDictionary*)raw_data{
    data = raw_data;
}

@end
