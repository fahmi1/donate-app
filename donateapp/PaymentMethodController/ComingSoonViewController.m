//
//  ComingSoonViewController.m
//  donateapp
//
//  Created by Dead Mac on 04/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "ComingSoonViewController.h"

@implementation ComingSoonViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* START */
    /* Code START Here */
    
    
    
    UILabel *label_instruction = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, self.view.size.height-100)];
    label_instruction.text = NSLocalizedString(@"COMING SOON", nil);
    label_instruction.textAlignment = NSTextAlignmentCenter;
    label_instruction.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:16];
    label_instruction.textColor = [UIColor lightGrayColor];
    
    [self.view addSubview:label_instruction];
    
    /* Code END */
    
    /* END CODE */
    
}
@end
