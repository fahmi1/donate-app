//
//  PaymentViewController.m
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "CreditCardViewController.h"

@implementation CreditCardViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* START */
    /* Code START Here */
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame), 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    [self.view addSubview:scroller];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [scroller addGestureRecognizer:singleTap];
    
    
    [self credit_card_content];
    
    [self total_box:[NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]]];
    
    [self pay_now_btn];
    
    [self instruction:NSLocalizedString(@"For a recurring donation, we will charge your credit card on the next billing date start from today's date and of course you can cancel anytime", nil)];
    
    [scroller layoutWithDuration:0.3 completion:nil];
    
    
    /* Code END */
    
    /* END CODE */
    
}


-(void)credit_card_content{
    
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 280)];
    
    UIView *paddingViewCard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *card_number_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width, 20)];
    card_number_text_label.text = NSLocalizedString(@"CARD NUMBER", nil);
    card_number_text_label.textAlignment = NSTextAlignmentLeft;
    card_number_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    card_number_text_label.textColor = [UIColor blackColor];
    
    [box addSubview:card_number_text_label];
    
    card_company_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width-20, 20)];
    card_company_text_label.textAlignment = NSTextAlignmentRight;
    card_company_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    card_company_text_label.textColor = [UIColor blackColor];
    
    [box addSubview:card_company_text_label];
    
    
    
    card_number = [[BKCardNumberField alloc] initWithFrame:CGRectMake(10, 40, box.width-20, 40)];
    card_number.leftViewMode = UITextFieldViewModeAlways;
    card_number.placeholder = NSLocalizedString(@"Enter your card number", nil);
    card_number.leftView = paddingViewCard;
    [card_number setKeyboardType:UIKeyboardTypeNumberPad];
    card_number.layer.borderColor=[[UIColor redColor]CGColor];
    card_number.layer.borderWidth= 1.0f;
    [card_number addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    card_number.delegate=self;
    card_number.showsCardLogo = YES;
    [box addSubview:card_number];
    
     UIView *paddingViewCardExpired = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *card_expired_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, box.width, 20)];
    card_expired_text_label.text = NSLocalizedString(@"CARD EXPIRY", nil);
    card_expired_text_label.textAlignment = NSTextAlignmentLeft;
    card_expired_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    card_expired_text_label.textColor = [UIColor blackColor];
    [box addSubview:card_expired_text_label];
    
    card_expired = [[BKCardExpiryField alloc] initWithFrame:CGRectMake(10, 130, box.width-20, 40)];
    card_expired.leftViewMode = UITextFieldViewModeAlways;
    card_expired.placeholder = NSLocalizedString(@"Enter your card expiry", nil);
    card_expired.leftView = paddingViewCardExpired;
    [card_expired setKeyboardType:UIKeyboardTypeNumberPad];
    card_expired.layer.borderColor=[[UIColor redColor]CGColor];
    card_expired.layer.borderWidth= 1.0f;
    card_expired.delegate=self;
    [box addSubview:card_expired];
    
    
    
    
    UIView *paddingViewCvv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    UILabel *cvv_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 190, box.width, 20)];
    cvv_text_label.text = NSLocalizedString(@"CVV", nil);
    cvv_text_label.textAlignment = NSTextAlignmentLeft;
    cvv_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    cvv_text_label.textColor = [UIColor blackColor];
    [box addSubview:cvv_text_label];
    
    cvv = [[UITextField alloc] initWithFrame:CGRectMake(10, 220, box.width-20, 40)];
    cvv.leftViewMode = UITextFieldViewModeAlways;
    cvv.placeholder = NSLocalizedString(@"Enter your card cvv", nil);
    cvv.leftView = paddingViewCvv;
    [cvv setKeyboardType:UIKeyboardTypeNumberPad];
    cvv.layer.borderColor=[[UIColor redColor]CGColor];
    cvv.layer.borderWidth= 1.0f;
    cvv.tag = 3;
    cvv.delegate=self;
    [box addSubview:cvv];
    
    
   
    [scroller.boxes addObject:box];
    
    
    
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField.tag == 3)
    {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 4;
    }
    else
    {
        return YES;
    }
}


-(void)instruction:(NSString*)value{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 60)];
    
    UILabel *instruction_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width-20, 60)];
    instruction_text_label.text = value;
    instruction_text_label.textAlignment = NSTextAlignmentCenter;
    instruction_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    instruction_text_label.textColor = [UIColor blackColor];
    instruction_text_label.lineBreakMode = NSLineBreakByWordWrapping;
    instruction_text_label.numberOfLines = 0;

    [box addSubview:instruction_text_label];
    
    [scroller.boxes addObject:box];
    
}


-(void)total_box:(NSString*)value{
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_sum_layout:self.view.size value:value];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    
}

-(void)pay_now_btn{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 70)];
    
    HyLoglnButton *log = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 30, box.width-20, 40)];
    [log setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:log];
    [log setTitle:NSLocalizedString(@"PAY NOW", nil) forState:UIControlStateNormal];
    [log addTarget:self action:@selector(pay_now:) forControlEvents:UIControlEventTouchUpInside];
    
    [scroller.boxes addObject:box];
    
}

-(void)pay_now:(HyLoglnButton*)button{
    
    BOOL isValid = [Luhn validateString:[card_number.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    if([card_number.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
            
            
        }];
    }
    else if([card_expired.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
            
            
        }];
    }
    else if([cvv.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
            
            
        }];
    }
    else if(isValid == false)
    {
        [button ErrorRevertAnimationCompletion:^{
            
            
            
        }];
        
        [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Your credit card is not valid. Please check again.", nil)closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSDictionary *checkout_data = [[CheckOutDataService instance] pay_now_credit_card:@"credit_card" credit_card:card_number.text cc_expiry:card_expired.text cvv:cvv.text ];
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                 int status = [[checkout_data objectForKey:@"status"] intValue];
                
                if(status == 1)
                {
                    [button RevertAnimationCompletion:^{
                        
                        
                        
                    }];
                    
                    [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:[checkout_data objectForKey:@"reason"]closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
                }
                else
                {
                    [[CartHelper instance] reset_cart];
                    [button RevertAnimationCompletion:^{
                        
                        
                        
                    }];
                    
                    
                    WebViewController *invoice = [[WebViewController alloc] init];
                    invoice.title = NSLocalizedString(@"INVOICED", nil);
                    [invoice set_url:[NSString stringWithFormat:@"%@invoiced/no/%@",BACKEND_MAIN_URL,[[[checkout_data objectForKey:@"data"] objectForKey:@"invoice_no"] lowercaseString]]];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:invoice];
                    
                    
                    [[[ViewControllerHelper instance] get_tabbar_controller] presentViewController:nav animated:YES completion:^{
                        
                        [[[ViewControllerHelper instance] get_cart_controller] refresh_content];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.9 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                         [[[ViewControllerHelper instance] get_tabbar_controller].navigationController popViewControllerAnimated:YES];
                            
                            [[UserDataService instance] refresh_user_data];
                            
                        });
                        
                       
                    }];
                    
                }
             
                
                
                
                
                
            });
            
        });
    }
   
}

- (void)textFieldEditingChanged:(id)sender
{
    if (sender == card_number) {
        
        NSString *cardCompany = card_number.cardCompanyName;
        if (nil == cardCompany) {
            if([card_number.text isEqualToString:@""])
            {
                cardCompany = @"";
            }
            else
            {
                 cardCompany = NSLocalizedString(@"UNKNOWN", nil);
            }
           
        }
        
        card_company_text_label.text = [cardCompany uppercaseString];
        
        
        
        
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    svos = scroller.contentOffset;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:scroller];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [scroller setContentOffset:pt animated:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)handleSingleTap:(id)sender{
     [card_number resignFirstResponder];
    [card_expired resignFirstResponder];
    [cvv resignFirstResponder];
}

@end
