//
//  BankTransferViewController.m
//  donateapp
//
//  Created by Dead Mac on 04/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "BankTransferViewController.h"

@implementation BankTransferViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* START */
    /* Code START Here */
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame), 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    [self.view addSubview:scroller];
    
   
    
    [self total_box:[NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]]];
    
    [self pay_now_btn];
    
    [self instruction:NSLocalizedString(@"For a recurring donation, we will send you an invoice and you need to pay manually unless you change your prefer method to credit card. Of course you can cancel any time", nil)];
    
    [scroller layoutWithDuration:0.3 completion:nil];
    
    
    /* Code END */
    
    /* END CODE */
    
}




-(void)instruction:(NSString*)value{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 80)];
    
    UILabel *instruction_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width-20, 80)];
    instruction_text_label.text = value;
    instruction_text_label.textAlignment = NSTextAlignmentCenter;
    instruction_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    instruction_text_label.textColor = [UIColor blackColor];
    instruction_text_label.lineBreakMode = NSLineBreakByWordWrapping;
    instruction_text_label.numberOfLines = 0;
    
    [box addSubview:instruction_text_label];
    
    [scroller.boxes addObject:box];
    
}


-(void)total_box:(NSString*)value{
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_sum_layout:self.view.size value:value];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    
}

-(void)pay_now_btn{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 70)];
    
    HyLoglnButton *log = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 30, box.width-20, 40)];
    [log setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:log];
    [log setTitle:NSLocalizedString(@"CREATE INVOICE NOW", nil) forState:UIControlStateNormal];
    [log addTarget:self action:@selector(pay_now:) forControlEvents:UIControlEventTouchUpInside];
    
    [scroller.boxes addObject:box];
    
}

-(void)pay_now:(HyLoglnButton*)button{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSDictionary *checkout_data = [[CheckOutDataService instance] pay_now_bank_transfer:@"bank_transfer" ];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            int status = [[checkout_data objectForKey:@"status"] intValue];
            
            if(status == 1)
            {
                [button RevertAnimationCompletion:^{
                    
                    
                    
                }];
                
                [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:[checkout_data objectForKey:@"reason"]closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
            }
            else
            {
                [[CartHelper instance] reset_cart];
                [button RevertAnimationCompletion:^{
                    
                    
                    
                }];
                
                
                WebViewController *invoice = [[WebViewController alloc] init];
                invoice.title = NSLocalizedString(@"INVOICED", nil);
                NSLog(@"%@",[NSString stringWithFormat:@"%@invoiced/no/%@",BACKEND_MAIN_URL,[[[checkout_data objectForKey:@"data"] objectForKey:@"invoice_no"] lowercaseString]]);
                [invoice set_url:[NSString stringWithFormat:@"%@invoiced/no/%@",BACKEND_MAIN_URL,[[[checkout_data objectForKey:@"data"] objectForKey:@"invoice_no"] lowercaseString]]];
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:invoice];
                
                
                [[[ViewControllerHelper instance] get_tabbar_controller] presentViewController:nav animated:YES completion:^{
                    
                    [[[ViewControllerHelper instance] get_cart_controller] refresh_content];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.9 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [[[ViewControllerHelper instance] get_tabbar_controller].navigationController popViewControllerAnimated:YES];
                         [[UserDataService instance] refresh_user_data];
                    });
                    
                    
                }];
                
            }
            
            
            
            
            
            
        });
        
    });
    
}

@end
