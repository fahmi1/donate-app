//
//  PreviousCCPaymentController.m
//  donateapp
//
//  Created by Dead Mac on 05/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "PreviousCCPaymentController.h"

@implementation PreviousCCPaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    /* START */
    /* Code START Here */
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame), 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    [self.view addSubview:scroller];
    
    [self single_box:[[UserDataService instance].user_data objectForKey:@"credit_card_truncate"] expiry:[[UserDataService instance].user_data objectForKey:@"credit_card_expiry"] ];
    
    [self total_box:[NSString stringWithFormat:@"%d",[[CartHelper instance] count_total]]];
    
    [self pay_now_btn];
    
    
    [scroller layoutWithDuration:0.3 completion:nil];
    
    
    /* Code END */
    
    /* END CODE */
    
}


-(void)total_box:(NSString*)value{
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_sum_layout:self.view.size value:value];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    
}

-(void)pay_now_btn{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 70)];
    
    HyLoglnButton *log = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 30, box.width-20, 40)];
    [log setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:log];
    [log setTitle:NSLocalizedString(@"PAY NOW", nil) forState:UIControlStateNormal];
    [log addTarget:self action:@selector(pay_now:) forControlEvents:UIControlEventTouchUpInside];
    
    [scroller.boxes addObject:box];
    
}


-(void)pay_now:(HyLoglnButton*)button{
    
   
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSDictionary *checkout_data = [[CheckOutDataService instance] pay_now_previous_credit_card];
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                int status = [[checkout_data objectForKey:@"status"] intValue];
                
                if(status == 1)
                {
                    [button RevertAnimationCompletion:^{
                        
                        
                        
                    }];
                    
                    [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:[checkout_data objectForKey:@"reason"]closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
                }
                else
                {
                    [[CartHelper instance] reset_cart];
                    [button RevertAnimationCompletion:^{
                        
                        
                        
                    }];
                    
                    
                    WebViewController *invoice = [[WebViewController alloc] init];
                    invoice.title = NSLocalizedString(@"INVOICED", nil);
                    [invoice set_url:[NSString stringWithFormat:@"%@invoiced/no/%@",BACKEND_MAIN_URL,[[[checkout_data objectForKey:@"data"] objectForKey:@"invoice_no"] lowercaseString]]];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:invoice];
                    
                    
                    [[[ViewControllerHelper instance] get_tabbar_controller] presentViewController:nav animated:YES completion:^{
                        
                        [[[ViewControllerHelper instance] get_cart_controller] refresh_content];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.9 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            [[[ViewControllerHelper instance] get_tabbar_controller].navigationController popViewControllerAnimated:YES];
                            
                            [[UserDataService instance] refresh_user_data];
                            
                        });
                        
                        
                    }];
                    
                }
                
                
                
                
                
                
            });
            
        });
    
    
}

-(void)single_box:(NSString*)card expiry:(NSString*)expiry{
    
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_layout:self.view.size charity_name:card value:@"" frequency:expiry];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    
    
    
    
    
    
    /* END cart delete btn */
}




@end
