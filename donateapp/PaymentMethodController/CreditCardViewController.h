//
//  PaymentViewController.h
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BKCardNumberField.h"
#import "BKCardExpiryField.h"
#import "CheckOutDataService.h"
#import "Luhn.h"
@interface CreditCardViewController : UIViewController<UITextFieldDelegate>
{
    MGScrollView *scroller;
    BKCardNumberField *card_number;
    BKCardExpiryField *card_expired;
    UITextField *cvv;
    UILabel *card_company_text_label;
    CGPoint svos;
}
@end
