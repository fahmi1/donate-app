//
//  LoginViewController.m
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "LoginViewController.h"

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    
    /* Code START Here */
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame), 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    [self.view addSubview:scroller];
    
    [self login_content];
    
    /* Code END */
}


-(void)login_content{
    
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 280)];
    
    UIView *paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *email_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width, 20)];
    email_text_label.text = NSLocalizedString(@"EMAIL", nil);
    email_text_label.textAlignment = NSTextAlignmentLeft;
    email_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    email_text_label.textColor = [UIColor blackColor];
    [box addSubview:email_text_label];
    
    email = [[UITextField alloc] initWithFrame:CGRectMake(10, 40, box.width-20, 40)];
    email.leftViewMode = UITextFieldViewModeAlways;
    email.placeholder = NSLocalizedString(@"Enter your email", nil);
    email.leftView = paddingViewEmail;
     [email setKeyboardType:UIKeyboardTypeEmailAddress];
    email.layer.borderColor=[[UIColor redColor]CGColor];
    email.layer.borderWidth= 1.0f;
    email.delegate=self;
    [box addSubview:email];
    
    
    UIView *paddingViewPass = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *pass_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, box.width, 20)];
    pass_text_label.text = NSLocalizedString(@"PASSWORD", nil);
    pass_text_label.textAlignment = NSTextAlignmentLeft;
    pass_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    pass_text_label.textColor = [UIColor blackColor];
    [box addSubview:pass_text_label];
    
    password = [[UITextField alloc] initWithFrame:CGRectMake(10, 120, box.width-20, 40)];
    password.placeholder = NSLocalizedString(@"Enter your password", nil);
    password.leftViewMode = UITextFieldViewModeAlways;
    password.leftView = paddingViewPass;
    password.layer.borderColor=[[UIColor redColor]CGColor];
    password.layer.borderWidth= 1.0f;
    password.delegate=self;
    password.secureTextEntry = YES;
    [box addSubview:password];
    
    
    
    
    HyLoglnButton *log = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 180, box.width-20, 40)];
    [log setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:log];
    [log setTitle:NSLocalizedString(@"LOGIN_BTN", nil) forState:UIControlStateNormal];
    [log addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    HyLoglnButton *facebook = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 240, box.width-20, 40)];
    [facebook setBackgroundColor:[UIColor colorWithRed:0.23 green:0.35 blue:0.6 alpha:1.0]];
    [box addSubview:facebook];
    [facebook setTitle:NSLocalizedString(@"LOGIN WITH FACEBOOK", nil) forState:UIControlStateNormal];
    [facebook addTarget:self action:@selector(login_with_facebook:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [scroller.boxes addObject:box];
    
    
    
     [scroller layoutWithDuration:0.3 completion:nil];
    
    
}



-(void)login:(HyLoglnButton *)button{
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    if([email.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Enter your email", nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
        
    }
    else if([password.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Enter your password", nil)closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else
    {
    
    
    
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSDictionary *user_data = [[UserDataService instance] login:email.text password:password.text];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if([[user_data objectForKey:@"status_code"] intValue] == 0)
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [button ExitAnimationCompletion:^{
                            //Register userdata in global
                            [[UserDataService instance] set_user_data:[user_data objectForKey:@"response"]];
                            
                            LoggedViewController *log = [[LoggedViewController alloc] init];
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
                            
                            FAKFontAwesome *myProfileIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
                            UIImage *myProfileIcon_image = [myProfileIcon imageWithSize:CGSizeMake(24, 24)];
                            nav.tabBarItem.image = myProfileIcon_image;
                            
                            UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
                            NSMutableArray *as = [tab.viewControllers mutableCopy];
                            [as replaceObjectAtIndex:1 withObject:nav];
                            
                            [[ViewControllerHelper instance] get_tabbar_controller].viewControllers = as;
                        }];
                    });
                    
                }
                else
                {
                    [button ErrorRevertAnimationCompletion:^{
                        
                        
                        
                    }];
                    
                    [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Your email/password is wrong", nil)closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
                }
                
                
            });
            
        });
        
        
    }
}


-(void)login_with_facebook:(HyLoglnButton *)button{
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:[[ViewControllerHelper instance] get_my_profile_controller_controller]
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             
             [button RevertAnimationCompletion:^{
                 
                 
                 
             }];
             
             [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Sorry error was occured while connecting with facebook. Please try again.", nil)closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
             
             
         } else if (result.isCancelled) {
            
             [button RevertAnimationCompletion:^{
                 
                 
                 
             }];
             
             
         } else {
             [self fetchUserInfo:button];
         }
     }];
    
    
    
    
}

-(void)fetchUserInfo:(HyLoglnButton *)button
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
           
             if (!error)
             {
                  NSString *fb_name = [result objectForKey:@"name"];
             NSString *fb_email = [result objectForKey:@"email"];
             NSString *fb_userId = [result objectForKey:@"id"];
       
                  
                 [self fb_server_login:button fb_id:fb_userId fb_email:fb_email fb_name:fb_name];
             }
             
             }];
    }
}

-(void)fb_server_login:(HyLoglnButton *)button fb_id:(NSString*)fb_id fb_email:(NSString*)fb_email fb_name:(NSString*)fb_name {
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSDictionary *user_data = [[UserDataService instance] login_fb:fb_id fb_email:fb_email fb_name:fb_name];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if([[user_data objectForKey:@"status_code"] intValue] == 0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [button ExitAnimationCompletion:^{
                        //Register userdata in global
                        [[UserDataService instance] set_user_data:[user_data objectForKey:@"response"]];
                        
                        LoggedViewController *log = [[LoggedViewController alloc] init];
                        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
                        
                        FAKFontAwesome *myProfileIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
                        UIImage *myProfileIcon_image = [myProfileIcon imageWithSize:CGSizeMake(24, 24)];
                        nav.tabBarItem.image = myProfileIcon_image;
                        
                        UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
                        NSMutableArray *as = [tab.viewControllers mutableCopy];
                        [as replaceObjectAtIndex:1 withObject:nav];
                        
                        [[ViewControllerHelper instance] get_tabbar_controller].viewControllers = as;
                    }];
                });
                
            }
            
            
        });
        
    });
    
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    svos = scroller.contentOffset;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:scroller];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [scroller setContentOffset:pt animated:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}



@end
