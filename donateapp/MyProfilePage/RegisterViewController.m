//
//  RegisterViewController.m
//  donateapp
//
//  Created by Dead Mac on 01/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "RegisterViewController.h"

@implementation RegisterViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    
    /* Code START Here */
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame)+40, 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    [self.view addSubview:scroller];
    [self.view addSubview:scroller];
    
    [self register_content];
    
    /* Code END */
}


-(void)register_content{
    
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 430)];
    
    UIView *paddingViewName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *name_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, box.width, 20)];
    name_text_label.text = NSLocalizedString(@"NAME", nil);
    name_text_label.textAlignment = NSTextAlignmentLeft;
    name_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    name_text_label.textColor = [UIColor blackColor];
    [box addSubview:name_text_label];
    
    name = [[UITextField alloc] initWithFrame:CGRectMake(10, 40, box.width-20, 40)];
    name.leftViewMode = UITextFieldViewModeAlways;
    name.placeholder = NSLocalizedString(@"Type your name", nil);
    name.leftView = paddingViewName;
    name.layer.borderColor=[[UIColor redColor]CGColor];
    name.layer.borderWidth= 1.0f;
    name.delegate=self;
    [box addSubview:name];
    
    
    UIView *paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *email_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, box.width, 20)];
    email_text_label.text = NSLocalizedString(@"EMAIL", nil);
    email_text_label.textAlignment = NSTextAlignmentLeft;
    email_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    email_text_label.textColor = [UIColor blackColor];
    [box addSubview:email_text_label];
    
    email = [[UITextField alloc] initWithFrame:CGRectMake(10, 120, box.width-20, 40)];
    email.placeholder = NSLocalizedString(@"Type your email", nil);
    email.leftViewMode = UITextFieldViewModeAlways;
    email.leftView = paddingViewEmail;
    [email setKeyboardType:UIKeyboardTypeEmailAddress];
    email.layer.borderColor=[[UIColor redColor]CGColor];
    email.layer.borderWidth= 1.0f;
    email.delegate=self;
    [box addSubview:email];
    
    
    
    UIView *paddingViewPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *password_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 180, box.width, 20)];
    password_text_label.text = NSLocalizedString(@"PASSWORD", nil);
    password_text_label.textAlignment = NSTextAlignmentLeft;
    password_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    password_text_label.textColor = [UIColor blackColor];
    [box addSubview:password_text_label];
    
    password = [[UITextField alloc] initWithFrame:CGRectMake(10, 210, box.width-20, 40)];
    password.placeholder = NSLocalizedString(@"Type your password", nil);
    password.leftViewMode = UITextFieldViewModeAlways;
    password.leftView = paddingViewPassword;
    password.layer.borderColor=[[UIColor redColor]CGColor];
    password.layer.borderWidth= 1.0f;
    password.secureTextEntry = YES;
    password.delegate=self;
    [box addSubview:password];
    
    
    UIView *paddingViewRePassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]; // Textfield Padding
    
    UILabel *Repassword_text_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 260, box.width, 20)];
    Repassword_text_label.text = NSLocalizedString(@"RETYPE PASSWORD", nil);
    Repassword_text_label.textAlignment = NSTextAlignmentLeft;
    Repassword_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:14];
    Repassword_text_label.textColor = [UIColor blackColor];
    [box addSubview:Repassword_text_label];
    
    Repassword = [[UITextField alloc] initWithFrame:CGRectMake(10, 290, box.width-20, 40)];
    Repassword.placeholder = NSLocalizedString(@"Type your password again", nil);
    Repassword.leftViewMode = UITextFieldViewModeAlways;
    Repassword.leftView = paddingViewRePassword;
    Repassword.layer.borderColor=[[UIColor redColor]CGColor];
    Repassword.layer.borderWidth= 1.0f;
    Repassword.secureTextEntry = YES;
    Repassword.delegate=self;
    [box addSubview:Repassword];
    
    
    UILabel *term_text_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 350, box.width, 20)];
    term_text_label.text = NSLocalizedString(@"REGISTER TERM", nil);
    term_text_label.textAlignment = NSTextAlignmentCenter;
    term_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:10];
    term_text_label.textColor = [UIColor blackColor];
    [box addSubview:term_text_label];
    
    
    HyLoglnButton *register_btn = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 380, box.width-20, 40)];
    [register_btn setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:register_btn];
    [register_btn setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateNormal];
    [register_btn addTarget:self action:@selector(register_action:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [scroller.boxes addObject:box];
    
    
    
    [scroller layoutWithDuration:0.3 completion:nil];
    
    
}


-(void)register_action:(HyLoglnButton *)button{
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    if([name.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Please enter your name", nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
        
    }
    else if(name.text.length < 2)
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Name must be more than 2 characters", nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else if([email.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Please enter your email", nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else if([[ToolHelper instance] validateEmail:email.text] == false)
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Email is not valid", nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
        
    }
    else if([password.text isEqualToString:@""])
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Please enter your password", nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else if(password.text.length < 6)
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Password must be at least 6 characters" , nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else if(password.text != Repassword.text)
    {
        [button ErrorRevertAnimationCompletion:^{
            
        }];
        
        [alert showError:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Your password & retype password does not match" , nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSDictionary *user_data = [[UserDataService instance] register_user:name.text email:email.text password:password.text];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                
                if([[user_data objectForKey:@"status_code"] intValue] == 5)
                {
                    [button ErrorRevertAnimationCompletion:^{
                        
                    }];
                    
                    [alert showWarning:self title:NSLocalizedString(@"ERROR", nil) subTitle:NSLocalizedString(@"Your email has been registered. Please use other email address." , nil) closeButtonTitle:NSLocalizedString(@"OK" , nil) duration:0.0f];
                    
                    
                }
                else if([[user_data objectForKey:@"status_code"] intValue] == 0)
                {
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [button ExitAnimationCompletion:^{
                            //Register userdata in global
                            [[UserDataService instance] set_user_data:[user_data objectForKey:@"response"]];
                            
                            LoggedViewController *log = [[LoggedViewController alloc] init];
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
                            
                            FAKFontAwesome *myProfileIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
                            UIImage *myProfileIcon_image = [myProfileIcon imageWithSize:CGSizeMake(24, 24)];
                            nav.tabBarItem.image = myProfileIcon_image;
                            
                            UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
                            NSMutableArray *as = [tab.viewControllers mutableCopy];
                            [as replaceObjectAtIndex:1 withObject:nav];
                            
                            [[ViewControllerHelper instance] get_tabbar_controller].viewControllers = as;
                        }];
                    });
                    
                    
                    
                
                
                }
                else
                {
                    [button ErrorRevertAnimationCompletion:^{
                        
                    }];
                }
            });
            
        });
        
        
    }
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    svos = scroller.contentOffset;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:scroller];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [scroller setContentOffset:pt animated:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

@end
