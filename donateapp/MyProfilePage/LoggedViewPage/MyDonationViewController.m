//
//  MyDonationViewController.m
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "MyDonationViewController.h"

@implementation MyDonationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling*/
    
    
    /* Start Code */
    
    //Register to global
    [[ViewControllerHelper instance] set_donation_controller:self];
    
    
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    scroller.bottomPadding = 100;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame)+30, 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    
    [self.view addSubview:scroller];
    
    /* ADD LABEL */
    label_instruction = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, self.view.size.height-100)];
    label_instruction.text = NSLocalizedString(@"INVOICE IS EMPTY", nil);
    label_instruction.textAlignment = NSTextAlignmentCenter;
    label_instruction.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:16];
    label_instruction.textColor = [UIColor lightGrayColor];
    label_instruction.hidden = YES;
    [self.view addSubview:label_instruction];
    
    
    [self content];
    
    /* End Code */
}


-(void)content{
    
    NSArray *array = [[UserDataService instance].user_data objectForKey:@"invoices"];
    
    for(int i=0;i<array.count;i++)
    {
        NSDictionary *inv_data = [array objectAtIndex:i];
        NSString *status;
        if([[inv_data objectForKey:@"status"] isEqualToString:@"2"])
        {
            status = @"PAID";
        }
        else
        {
            status = @"PENDING";
        }
        
        [self single_box:[NSString stringWithFormat:@"%@ - %@",[inv_data objectForKey:@"invoice_no"],status] invoice_no:[inv_data objectForKey:@"invoice_no"]  date:[NSString stringWithFormat:@"%@ | %@",[inv_data objectForKey:@"created"],[inv_data objectForKey:@"payment_method"]] value:[inv_data objectForKey:@"total"]];
    }
    
    if(array.count == 0)
    {
        label_instruction.hidden = NO;
    }
    else
    {
         label_instruction.hidden = YES;
    }
    
    [scroller layoutWithDuration:0.3 completion:nil];
}

-(void)refresh_content{
    [scroller.boxes removeAllObjects];
    [self content];
}

-(void)single_box:(NSString*)invoice_no_and_status invoice_no:(NSString*)inv_no date:(NSString*)date value:(NSString*)value{
    
   
    
    LayoutBoxHelper *box = [LayoutBoxHelper cart_layout:self.view.size charity_name:invoice_no_and_status value:value frequency:date];
    
   
    [scroller.boxes addObject:box];
    
    
    box.onTap = ^{
        
        WebViewController *invoice = [[WebViewController alloc] init];
        invoice.title = NSLocalizedString(@"INVOICED", nil);
        
        [invoice set_url:[NSString stringWithFormat:@"%@invoiced/no/%@",BACKEND_MAIN_URL,[inv_no lowercaseString]]];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:invoice];
        
        
        [[[ViewControllerHelper instance] get_tabbar_controller] presentViewController:nav animated:YES completion:nil];
        
    };
    
    
}


@end
