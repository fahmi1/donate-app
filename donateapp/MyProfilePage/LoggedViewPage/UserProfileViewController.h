//
//  UserProfileViewController.h
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutBoxHelper.h"
#import "MyProfileController.h"
@interface UserProfileViewController : UIViewController
{
    MGScrollView *scroller;
}
-(void)refresh_content;
@end
