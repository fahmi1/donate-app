//
//  UserProfileViewController.m
//  donateapp
//
//  Created by Dead Mac on 03/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "UserProfileViewController.h"

@implementation UserProfileViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling*/
    
    
    /* Start Code */
    
    //Register to global
    [[ViewControllerHelper instance] set_user_controller:self];
    
    //Add background in viewcontroller
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 140)];
    background.image = [UIImage imageNamed:@"assets/user-profile-background.jpg"];
    [self.view addSubview:background];
    
    scroller = [MGScrollView scroller];
    scroller.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    scroller.alwaysBounceVertical = YES;
    scroller.bottomPadding = 100;
    
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight([[ViewControllerHelper instance] get_tabbar_controller].tabBar.frame)+30, 0);
    scroller.contentInset = adjustForTabbarInsets;
    scroller.scrollIndicatorInsets = adjustForTabbarInsets;
    
    [self.view addSubview:scroller];
    
    [self content];
    /* End Code */
}


-(void)content{
    [self header_content];
    [self user_image];
    
    [self display_data:@"fa-user" label:@"NAME" value:[[UserDataService instance].user_data objectForKey:@"name"]];
    [self display_data:@"fa-envelope" label:@"EMAIL" value:[[UserDataService instance].user_data objectForKey:@"email"]];
    [self display_data:@"fa-heartbeat" label:@"DONATED" value:[NSString stringWithFormat:@"%@ %@",[[Appearances instance] default_currency],[[UserDataService instance].user_data objectForKey:@"total"]]];
    NSLog(@"%@",[[UserDataService instance].user_data objectForKey:@"total"]);
    
    [self logout_btn];
    [scroller layoutWithDuration:0.3 completion:nil];
}

-(void)refresh_content{
    [scroller.boxes removeAllObjects];
    [self content];
}

-(void)header_content{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 10, self.view.size.width, 50)];
    
    
    UILabel *name_text_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, box.width, 20)];
    name_text_label.text = [[UserDataService instance].user_data objectForKey:@"name"];
    name_text_label.textAlignment = NSTextAlignmentCenter;
    name_text_label.font = [UIFont fontWithName:[[Appearances instance] font_family_bold] size:20];
    name_text_label.textColor = [UIColor whiteColor];
    [box addSubview:name_text_label];
    
    [scroller.boxes addObject:box];
    
}


-(void)user_image{
   
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 10, self.view.size.width, 170)];
    APAvatarImageView *avatarImageView = [[APAvatarImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    avatarImageView.center = CGPointMake(self.view.size.width/2,90);
    avatarImageView.borderColor = [UIColor whiteColor];
    avatarImageView.borderWidth = 3.0;
    APAvatarImageView *avatarImageView2 =avatarImageView;
    [avatarImageView setImageWithURL:[NSURL URLWithString:[[UserDataService instance].user_data objectForKey:@"profile_image"]]
                    placeholderImage:[UIImage imageNamed:NSLocalizedString(@"image_loading_placeholder", nil)]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                               
                            
                               avatarImageView2.image = [[ToolHelper instance] imageByScalingAndCroppingForSize:CGSizeMake(100, 100) source:image];
                           }];
    
    
    [box addSubview:avatarImageView];
    [scroller.boxes addObject:box];
    
}


-(void)display_data:(NSString*)icon label:(NSString*)label value:(NSString*)value{
    
    
    
    LayoutBoxHelper *box = [LayoutBoxHelper user_display_data:self.view.size fa_icon:icon label:label value:value];
    
    //box.leftMargin = 5;
    [scroller.boxes addObject:box];
    
    
    
}


-(void)logout_btn{
    
    MGBox *box = [[MGBox alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 70)];
    
    HyLoglnButton *log = [[HyLoglnButton alloc] initWithFrame:CGRectMake(10, 30, box.width-20, 40)];
    [log setBackgroundColor:[[Appearances instance] nav_bar_color]];
    [box addSubview:log];
    [log setTitle:NSLocalizedString(@"LOGOUT", nil) forState:UIControlStateNormal];
    [log addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    
    [scroller.boxes addObject:box];
    
}

-(void)logout:(HyLoglnButton *)button{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [[UserDataService instance] logout:[[UserDataService instance].user_data objectForKey:@"hash_id"]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [button ExitAnimationCompletion:^{
                    
                    //We remove data onthe device
                    [[FileSaveHelper instance] deleteArrayFile:@"user_data"];
                    
                    
                    //then we logout fb if log using fb 
                    if([[[UserDataService instance].user_data objectForKey:@"registered_using"] isEqualToString:@"facebook"])
                    {
                        [[FBSDKLoginManager new] logOut];
                        
                    }
                    //then we open login controller
                    MyProfileController *log = [[MyProfileController alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
                    
                    FAKFontAwesome *myProfileIcon = [FAKFontAwesome  iconWithIdentifier:@"fa-user" size:24 error:nil];
                    UIImage *myProfileIcon_image = [myProfileIcon imageWithSize:CGSizeMake(24, 24)];
                    nav.tabBarItem.image = myProfileIcon_image;
                    
                    UITabBarController *tab = [[ViewControllerHelper instance] get_tabbar_controller];
                    NSMutableArray *as = [tab.viewControllers mutableCopy];
                    [as replaceObjectAtIndex:1 withObject:nav];
                    
                    [[ViewControllerHelper instance] get_tabbar_controller].viewControllers = as;
                }];
            });
            
            
        });
        
    });

}



@end
