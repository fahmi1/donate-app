//
//  LoggedViewController.m
//  donateapp
//
//  Created by Dead Mac on 02/01/2016.
//  Copyright © 2016 Fahmi. All rights reserved.
//

#import "LoggedViewController.h"
@interface LoggedViewController ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end
@implementation LoggedViewController

-(id)init{
    self = [super init];
    if(self) {
        
        [self setTitle:NSLocalizedString(@"PROFILE", nil)];
        
        
    }
    
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [[ViewControllerHelper instance] get_tabbar_controller].navigationItem.title=NSLocalizedString(@"PROFILE", nil);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* START View Controller Styling */
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[[Appearances instance] nav_bar_font_color]}];
    
    self.navigationController.navigationBar.barTintColor = [[Appearances instance] nav_bar_color];
    [self.view setBackgroundColor:[[Appearances instance] view_controller_background_color]];
    /* END View Controller Styling */
    
    
    
    //Register into Helper
    [[ViewControllerHelper instance] set_logged_controller:self];
    
    NSMutableArray *controllerArray = [NSMutableArray array];
    
    UserProfileViewController *user_profile = [[UserProfileViewController alloc] init];
    user_profile.title = NSLocalizedString(@"ME", nil);
    [controllerArray addObject:user_profile];
    
    MyDonationViewController *my_donation = [[MyDonationViewController alloc] init];
    my_donation.title = NSLocalizedString(@"INVOICES", nil);
    [controllerArray addObject:my_donation];
    
    RecurrenceViewController *re_donation = [[RecurrenceViewController alloc] init];
    re_donation.title = NSLocalizedString(@"RECURRENCE", nil);
    [controllerArray addObject:re_donation];
    
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor redColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:[[Appearances instance] font_family_regular] size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(100.0),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    // Initialize page menu with controller array, frame, and optional parameters
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 63, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    
    /* END CODE */
    
}





@end
