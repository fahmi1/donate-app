<b>EZ Donate iOS Mobile App</b>

<h3>INTRODUCTION</h3>
<p>
Hello and thank you for visiting my profile. I created this project is to show my capabilities  to any companies before hiring me.
It will verify first phase of your hiring process that i am genuine Native iOS Developer. Of course this project  is a
basic Donate Application. A lot of improvement can be done. It took me 5 days to completed this sample (check my commit for verification). It will give you an idea my coding dna and written style.
Again, feel free to browse and download this project. Thank you.
</p>
<p>Download My Resume https://www.dropbox.com/s/fsx4j4clqpjc9q2/fahmi_cv.docx?dl=0</p>
<h3>EZ DONATE</h3>
<p>
Ez donate is the application to donate multiple organisation at same time. Fast,Easy and infomative.<br>
Development directions :<br>
- Less click as possible<br>
- Infomative in small area.<br>
- Fast<br>
- Recurring Donation (optional)
- Organization data was pulled from http://everydayhero.com API (JSON format)
</p>

<h3>HOW DOES IT WORKS ?</h3>
<p>
1) User find an organisation<br>
2) Choose amount to donate<br>
3) Pay via credit card (Intergrated with Authorize.net)<br><br>
View demo for detail
</p>

<h3>BACKEND</h3>
<p>
This demo already setup with support backend. You can view back end source code at <br>
https://gitlab.com/fahmi1/donate-app-backend
</p>

<h3>DEMO</h3>
<p>
Watch at https://youtu.be/NGtFHQVwcl4
</p>


<p>
<h3>ENVIROMENTS</h3>
1. Xcode 7.2<br>
2. GitLab<br>
3. OSX - 10.11.12
</p>
<p>

<p>
<h3>HOW TO RUN DEMO</h3>
1. Open xcode<br>
2. Please download latest facebookSDK and add into project. It will throw error.FBSDKLoginKit & FBSDKCoreKit<br>
2. Run Simulator/device
</p>
<p>
<h3>BASIC EXPLANATION</h3> 
1. Demo is use http://everydayhero.com API to display charities data<br>
2. Backend Demo is run at my server at http://donateapp.jobinic.com/ (<a href="https://gitlab.com/fahmi1/donate-app-backend">Learn more</a>)<br>
3. Credit card payment gateway is use Authorize.net sandbox & connected with my sandbox account. I will receive email for every purchase made.<br>
4. You can use sample credit card by Authorize.net <a href="https://community.developer.authorize.net/t5/Integration-and-Testing/Test-Credit-Card-Numbers/td-p/7653">(here)</a><br>
5. Currently, 2 types of payment method is supported. Credit Card & Bank Transfer.<br>
6. Basic Story Line Browse Charities->Choose Charity->Select Donation Value->Checkout->Pay<br>
</p>
<p>
<h3>SETUP</h3>
1. Go to GlobalSettings.h . Setup backend url, api key, everyday hero api<br>
2. Go to core/Appearences.h to setup basic appearence in App<br>
3. Go to Supporting Files/Localizable.strings to change to other language<br>
4. Go to Info.plist to setup facebook ID<br>
</p>

<p>
<h3>FRAMEWORKS</h3>
1. FBSDKLoginKit.framework<br>
2. FBSDKCoreKit.framework<br>
3. CoreText.framework<br>
4. CoreGraphics.framework<br>
</p>

<p>
<h3>3RD PARTY</h3>
1. LohnAlgo<br>
2. BKMoneyKit - https://github.com/bkook/BKMoneyKit<br>
3. APAvatarImageView - https://github.com/ankurp/APAvatarImageView<br>
4. HyLoginButton<br>
5. SCLAlertView - https://github.com/dogo/SCLAlertView<br>
6. DKScrollingTabController - https://github.com/dkhamsing/DKScrollingTabController<br>
7. MaryPopin - https://github.com/Backelite/MaryPopin<br>
8. MGBoxKit - https://github.com/sobri909/MGBoxKit<br>
9. SDWebImage - https://github.com/rs/SDWebImage<br>
10. SBJson - https://github.com/stig/json-framework/<br>
11. CAPSPageMenu - https://github.com/uacaps/PageMenu<br>
12. FontAwesomeKit - https://github.com/PrideChung/FontAwesomeKit<br>
</p>

